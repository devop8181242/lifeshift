 export const Autorizado=(Component,Navigate,usu)=>{
    return localStorage.getItem('token') &&
    usu.usuario.confirmar && usu.usuario.confirmar.data[0] === 1 ? (
    <Component />
  ) : (
    <Navigate to='/verificacion' />
  );
}
import {format,register } from 'timeago.js'
import esLocale from 'timeago.js/lib/lang/es';  // Asegúrate de importar el archivo de idioma correspondiente

 export const Helpers={

}
Helpers.timeago=(timestamp)=>{

   register('my-locale', {
      prefix: 'hace',
      suffix: '',
      seconds: 'menos de un minuto',
      minute: 'un minuto',
      minutes: '%d minutos',
      hour: 'una hora',
      hours: '%d horas',
      day: 'un día',
      days: '%d días',
      month: 'un mes',
      months: '%d meses',
      year: 'un año',
      years: '%d años',
    });

    // Usa la configuración personalizada
    register('my-locale', esLocale);  // También puedes basarte en un idioma existente, en este caso, 'es' para español
   return format(timestamp, 'my-locale')
}//dentro de helpers creo un metodo que me formatea fechas

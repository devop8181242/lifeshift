import React, { useEffect, useState, useContext } from 'react';
import { Navegacionlog } from '../otras paginas/navegacionloguiado';
import { content } from '../contexto'
import $, { map } from 'jquery';
import { Helpers } from './handlbars'
import { Normal } from '../otras paginas/volveralanormalidad';

import axios from 'axios'


import '../general.css'
export function Medicamento_historial({ navigata }) {
  const [histo, sethisto] = useState([])
  const navigate = navigata();
  const { usu } = useContext(content)
  useEffect(() => {
    const histome = async () => {
      if (usu.usuario) {
        try {
          const res = await axios.post('http://localhost:3001/historialmedicamento', { usuario: usu.usuario })
          console.log(res.data)
          sethisto(res.data)

        } catch (error) {
          console.log(error)
        }

      } else {
        if (!localStorage.getItem('token')) {
          navigate('/')
        }


      }

    }
    histome()

  }, [usu])


  const formatFechaResetado = (timestamp) => {
    const date = new Date(timestamp);
    const options = { day: 'numeric', month: 'long', year: 'numeric', hour: 'numeric', minute: 'numeric', hour12: true };
    const formattedDate = date.toLocaleDateString('es-ES', options);

    return formattedDate.replace(/(\d{1,2}:\d{2})/, (match, time) => {
      if (time.startsWith('0')) {
        time = time.replace(/^0/, '12');
      }
      return time;
    }).replace(',', ' a las');
  };
  return (


    <>
      <div className='divhistorialmedi'>
        <div className='divhislog'>
          <Navegacionlog />
        </div>
        <div className='histodiv' onClick={Normal}>
          {

            !histo ? <div className='histomedicamento'><div>no tienes medicamentos</div> </div> : histo.length === 0 ? <div className='histomedicamento'><div>no tienes medicamentos</div> </div> : <table class="table">
              <thead class='tablamedicamento'>
                <tr>
                  <th scope="col">medicamentos</th>
                  <th scope="col">fecha_resetado</th>
                  <th scope="col">estado</th>
                  <th scope="col">cantidadresetada</th>
                  <th scope="col">dosis</th>
                  <th scope="col">mas_informacion</th>
                </tr>
              </thead>
              <tbody>
                {Array.isArray(histo) && histo?.map((his, key) => {
                  return <tr className='trhis'>

                    <th>{his.medicamento}</th>
                    <td>{formatFechaResetado(his.fecha_resetado)}</td>
                    <td>{his.estado}</td>
                    <td>{his.cantidad}</td>
                    <td>{his.dosis}</td>
                    <td><button className='btn btn-success button' onClick={() => {
                      let conta = 0
                      const ele = document.querySelector('.divhistorialmedi')
                      const elenuw = document.createElement('div')
                      const botoncerrar = document.createElement('button')
                      const elechildnuw = document.createElement('div')
                      const elemignuw = document.createElement('img')
                      const mulele = ['tr', 'tr', 'tr']
                      const muletd = ['th', 'th', 'th']
                      const fragmento = document.createDocumentFragment()
                      for (let mu of mulele) {
                        if (conta === 0) {
                          let ef = document.createElement(mu)
                          let ed = document.createElement(muletd[conta])
                          his.estado === 'reclamado' ? ed.innerHTML = ` reclamado <br> ${formatFechaResetado(his.fecha_reclamado)}` : ed.innerHTML = his.estado
                          ed.classList.toggle('celdalogme')

                          ef.classList.toggle('filalogme')
                          ef.appendChild(ed)
                          fragmento.appendChild(ef)

                          console.log(conta)
                        } else if (conta === 1) {
                          let ef = document.createElement(mu)
                          let ed = document.createElement(muletd[conta])
                          ed.innerHTML = ` resetado ${Helpers.timeago(his.fecha_resetado)}`
                          ed.classList.toggle('celdalogme')

                          ef.classList.toggle('filalogme')
                          ef.appendChild(ed)
                          fragmento.appendChild(ef)

                          console.log(conta)
                        } else if (conta === 2) {
                          let ef = document.createElement(mu)
                          let ed = document.createElement(muletd[conta])
                          ed.innerHTML = ` ingerir o aplicar cada: ${his.lapsotiempo} horas`
                          ed.classList.toggle('celdalogme')

                          ef.classList.toggle('filalogme')
                          ef.appendChild(ed)
                          fragmento.appendChild(ef)

                          console.log(conta)
                        }

                        conta++

                      }


                      elemignuw.classList.toggle('elegmignuw')
                      elechildnuw.classList.toggle('elechildnuw')
                      elemignuw.src = 'http://localhost:3001/dbimages/' + `${his.id_medicamento}monkeywit.png`
                      botoncerrar.classList.toggle('botoncerrar')
                      botoncerrar.textContent = 'Χ'

                      botoncerrar.style.position = 'absolute'
                      botoncerrar.style.right = '0'
                      botoncerrar.style.top = '0'

                      botoncerrar.style.padding = '2px 30px'

                      botoncerrar.addEventListener('click', () => {
                        ele.removeChild(elenuw)
                      })
                      elenuw.classList.toggle('condivimghis')
                      elechildnuw.appendChild(elemignuw)
                      elechildnuw.appendChild(fragmento)
                      elenuw.appendChild(elechildnuw)
                      elenuw.appendChild(botoncerrar)
                      ele.appendChild(elenuw)



                    }}>ver</button></td>
                  </tr>
                })}
              </tbody>
            </table>
          }
        </div>
      </div>
    </>

  );
}
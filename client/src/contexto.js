import { createContext, useState, useEffect } from 'react'//mecrea cun context el cual contendra todos los elementos de mi aplicacion
import Axios from 'axios';

export const content = createContext()
export function Taskcontentprovider(props) {
  const [paciente, setpaciente] = useState(false)
  const [usu, setusuasrio] = useState({
    usuario: '',
    tipo: ''
  })

  const [datpa, setpa] = useState({
    numerodocumento: '',
    nombre: '',
    apellido: '',
    motivo: '',
  })


  const [numerodocupa, setnumerodocu] = useState('')
  const cambiarestpa = (data) => {
    setpa(data)

  }
  const numerosetdo = (estado) => {
    setpaciente(estado)
  }
  useEffect(() => {
    if (localStorage.getItem('token')) {
      const od = async () => {
        try {
          const response = await Axios.get('http://localhost:3001/logiado', {
            headers: {
              'Authorization': `Bearer ${localStorage.getItem('token')}`
            }
          });


          setusuasrio({ ...usu, usuario: response.data.dato, tipo: response.data.tipo })






        } catch (error) {
          console.log(error.response.data.mensaje);

        }
      }
      od()


    }

  }, [])


  return <content.Provider value={{ usu, paciente, datpa, cambiarestpa, numerosetdo }}>{props.children}</content.Provider>
}

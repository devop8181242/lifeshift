import './general.css'
export function Navegacion() {
  const token = localStorage.getItem('token')

  return (
    <>
      <div className="navbar navbar-expand-lg  fixed-top  navegacion">
        <ul className="navbar-nav ">

          {!token && <a className="nav-item navbar-brand  imagennav" href="/"  >
            <li className="itemnav">
              <img className='imagenlogo' src="/logo_life shift.png " alt="Logo del sitio" />
              <span className='espanima'>life_shift</span>
            </li>
          </a>}

          {token && <a className="nav-item navbar-brand  imagennav" href="/"  >
            <li className="itemnav">
              <img className='imagenlogo' src="/logo_life shift.png " alt="Logo del sitio" />

            </li>
          </a>}

        </ul>


        {token && <ul className="navbar-nav ms-auto">
          <li className="nav-item navbar-brand">
            <button className='itemnav  botonnav' onClick={() => {
              localStorage.removeItem('token')
              window.location.reload()
            }}>salir</button>
          </li>
        </ul>}


      </div>
    </>
  )
}
import { FaUser } from "react-icons/fa";
import { IoMdHome } from "react-icons/io";
import { MdOutlineMedication } from "react-icons/md";
import { TfiAlignJustify } from "react-icons/tfi";
import { FaUserDoctor } from "react-icons/fa6";
import { content } from "../contexto"
import { useContext, useEffect } from "react";
import { Link } from "react-router-dom";
export function Navegacionlog() {
  const { usu } = useContext(content)
  useEffect(() => {

    console.log(usu)

  }, [usu])

  return (
    <div className="containernavlog">



      <ul className="listalog" style={{ padding: '0' }} >
        <a href="/historialmedicamento" style={{ textDecoration: 'none', color: 'black' }}>
          <li className="linavlog" onMouseEnter={() => {

            const me = document.querySelectorAll('.linavlog')
            const me2 = document.querySelector('.logmedicamento')

            if (me.length > 0) {
              const ele = document.createElement('div');
              ele.classList.add('spanuserlog');
              ele.innerHTML = 'medicamentos_resetados';
              ele.style.display = 'inline-block';
              ele.style.position = 'relative';
              ele.style.left = '50px'
              ele.style.top = '10px'
              ele.style.width = '50px'
              //ele.style.background='black'

              ele.style.textAlign = 'center'

              me[0].appendChild(ele);
            }


            me2.style.position = 'relative'
            me2.style.left = '30px'
            me[0].style.width = '40vh';
            me[0].style.background = '#ABB2B9';
            me[0].style.border = 'none';
            me[0].style.borderTopRightRadius = '20px';
            me[0].style.borderBottomRightRadius = '20px';
            me[0].style.zIndex = '300';
            me[0].style.display = 'flex';



          }} onMouseLeave={() => {
            const me = document.querySelectorAll('.linavlog')
            const el = document.querySelector('.spanuserlog')
            const me2 = document.querySelector('.logmedicamento')
            if (el) {
              me[0].removeChild(el)
              me[0].style.width = '';
              me[0].style.background = '';
              me[0].style.display = '';
              me2.style.position = ''
              me2.style.left = ''
              me[0].style.borderTopRightRadius = '';
              me[0].style.borderBottomRightRadius = '';
              me[0].style.border = '1px solid';

            }

          }} >

            <i className="logmedicamento">
              <MdOutlineMedication className="logemo" />
            </i>

          </li>
        </a>
        <a href="/Usuario" style={{ textDecoration: 'none', color: 'black' }}>
          <li className="linavlog" onMouseEnter={() => {

            const me = document.querySelectorAll('.linavlog')
            const me2 = document.querySelector('.logoperfil')



            if (me.length > 0) {
              const ele = document.createElement('div');


              ele.classList.add('spanuserlog');
              ele.innerHTML = 'perfil';
              //ele.style.width = '50px';
              ele.style.display = 'inline-block';
              ele.style.position = 'relative';
              ele.style.left = '50px'
              ele.style.width = '50px'
              ele.style.top = '10px'
              //ele.style.background='black'

              ele.style.textAlign = 'center'


              me[1].appendChild(ele);

            }

            me2.style.position = 'relative'
            me2.style.left = '20px'
            me[1].style.width = '30vh';
            me[1].style.background = '#ABB2B9';
            me[1].style.border = 'none';
            me[1].style.borderTopRightRadius = '20px';
            me[1].style.borderBottomRightRadius = '20px';
            me[1].style.zIndex = '300';
            me[1].style.display = 'flex';

            //me[0].style.margin = '20px';



          }} onMouseLeave={() => {
            const me = document.querySelectorAll('.linavlog')
            const el = document.querySelector('.spanuserlog')
            const me2 = document.querySelector('.logoperfil')
            if (el) {

              me[1].removeChild(el)
              me[1].style.width = '';
              me[1].style.background = '';
              me[1].style.display = '';
              me2.style.position = ''
              me2.style.left = ''
              me[1].style.borderTopRightRadius = '';
              me[1].style.borderBottomRightRadius = '';
              me[1].style.border = '1px solid';
            }

          }} >
            <i className="logoperfil">
              <FaUser className="logemo" />
            </i>
          </li>
        </a>
      </ul>

      <div className="movillog">
        <TfiAlignJustify className="logoburgue" onMouseDown={() => {
          console.log('hola')
          const elechild = document.querySelector('.listalog2')
          const elechild2 = document.querySelector('.containernavlog')

          const elechidlog2 = document.querySelector('.listalog2')



          const elechild3 = document.querySelector('.logoburgue')
          elechild3.style.display = "none"



          elechidlog2.style.position = 'absolute'
          elechidlog2.style.top = '0'
          elechidlog2.style.zIndex = '10'
          elechidlog2.style.background = 'gray'
          elechidlog2.style.height = '100%'

          elechild2.style.position = "fixed"
          elechild2.style.background = "gray"
          elechild2.style.width = "300px"


          elechild2.style.height = "100vh"
          elechild.style.display = "flex"






        }} />
      </div>


      <ul className="listalog2"  >
        <li className="linavlog2"  >
          <a href="/historialmedicamento" style={{ textDecoration: 'none', color: 'black' }}>
            <i className="logmedicamento2">
              <MdOutlineMedication className="logemo2" />
              <span>medicamentos</span>
            </i>
          </a>
        </li>

        <li className="linavlog2"   >
          <a href="/usuario" style={{ textDecoration: 'none', color: 'black' }}>
            <i className="logoperfil2">
              <FaUser className="logemo2" />
              <span>perfil</span>
            </i>
          </a>
        </li>

        {usu.tipo === 'medico' ? <li style={{ listStyle: 'none' }} className="forusu">
          <a href="/formula" style={{ textDecoration: 'none', color: 'black' }}>
            <i className="logoperfil2">
              <FaUserDoctor />
              <span >formular</span>
            </i>
          </a>
        </li> : <></>}

        {
          usu.tipo === 'farmaceutico' ? <li style={{ listStyle: 'none' }}>
            <Link to={`/usuariopendiente`} style={{ textDecoration: 'none', color: 'black' }}>
              <FaUserDoctor />
              <span >entregar</span>
            </Link>
          </li> : <></>
        }
      </ul>

    </div>
  )

}
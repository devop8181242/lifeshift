import { content } from '../contexto'
import { useState, useContext } from 'react'
import { useNavigate } from 'react-router-dom'
import axios from 'axios'

export function Estado() {

    const [inpot, setinpot] = useState({
        codi: ''
    })
    const { usu } = useContext(content)

    return (
        <div className="containercodigo">
            <div className="codigodeverificacio">
                <div className="divimagencodigo">
                    <img src="/logo_life shift.png" className="imagencodi" />
                </div>
                <form className="formcodigo" onSubmit={(e) => {
                    e.preventDefault()

                    if (usu.usuario.codigo === parseInt(inpot.codi)) {
                        const usumodificado = async () => {
                            try {
                                console.log('Antes de la llamada a Axios');
                                const mo = await axios.post('http://localhost:3001/modificarestado', { usu })
                                console.log(mo);
                                window.location.reload();
                            } catch (error) {
                                console.log(error)
                            }


                        }

                        usumodificado()



                    }
                }}>
                    <label className="labelcodigo">introduce tu codigo de verificacion</label>
                    <input type='text' className="inputcodigo" size="5" maxLength="4" name='codi' onChange={(e) => {
                        setinpot({ ...inpot, [e.target.name]: e.target.value })

                    }} />
                    <button role='submit' >verificar</button>
                </form>
                <div className="otrosbotones_codigo">
                    <button>enviar codigo por sms</button>  <button>enviar codigo por gmail</button>
                </div>
            </div>
        </div>
    )
}
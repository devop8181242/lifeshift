import { Routes, Route, BrowserRouter as Router, Navigate, useNavigate, useLocation } from 'react-router-dom'
import { Navegacion } from './navegacion'
import { Ingreso } from './paginaentrada/inicio'
import { Formula } from './usuarios/formula'
import { Usuariocomun } from './usuarios/usuariocomun';
import { Loguiar } from './utenticacion/loguiar';
import { Carrito } from './otras paginas/carrito'
import { Regitrate } from './utenticacion/registro'
import { Estado } from './verificacion/estado';
import { useState, useContext, useEffect } from 'react'//use context para poder utilizar el contexto
import { content } from './contexto'
import { Medicamento_historial } from './medicamentos/historial_medicamento';
import { Informacion } from './usuarios/informacion';
import { Autorizado } from './paginas_autorizadas/autorizadas'
import { Resetar } from './usuarios/resetar';
import { Pagina404 } from './pagina404';
import { Usuariopendiente } from './usuarios/usuariopendiente';
import { Confirmar } from './usuarios/confirmar';


export function Rotas() {
  const { usu } = useContext(content)
  const [isMounted, setIsMounted] = useState(false);
  const excludedRoutes = ['/', '/loguiar', '/usuario', '/verificacion', '/registro', '/informacion', '/formula', '/resetar', '/historialmedicamento', '/usuariopendiente', '/confirmar'];

  const navigate = useLocation()

  console.log(window.location.pathname)
  localStorage.getItem('token') && usu.usuario.confirmar && usu.usuario.confirmar.data[0] === 1 ? console.log('verdadero') : console.log('falso')
  return (
    <>

      {!excludedRoutes.includes(window.location.pathname.toLowerCase()) ? <></> : <Navegacion />
      }


      <Routes>
        <Route path='/loguiar' element={!localStorage.getItem('token') ? <Loguiar /> : <Navigate to='/Usuario' />} />
        <Route path='/' element={!localStorage.getItem('token') ? <Ingreso /> : <Navigate to='/Usuario' />} />
        <Route path='/verificacion' element={localStorage.getItem('token') &&
          usu.usuario.confirmar && usu.usuario.confirmar.data[0] === 0 ? <Estado /> : !localStorage.getItem('token') ? <Navigate to='/' /> : <Navigate to='/Usuario' />} />
        <Route
          path='/registro'
          element={
            !localStorage.getItem('token') ?
              <Regitrate />
              : <Navigate to='/Usuario' />
          }
        />
        <Route path='/informacion' element={<Informacion navigate={useNavigate} />} />
        <Route
          path='/Usuario' element={localStorage.getItem('token') ? <Usuariocomun /> : <Navigate to='/' />}
        />

        <Route path='/formula' element={<Formula />} />
        <Route path='/resetar' element={<Resetar />} />

        <Route path='/historialmedicamento' element={<Medicamento_historial navigata={useNavigate} />} />
        <Route path='/usuariopendiente' element={<Usuariopendiente />} />
        <Route path='/confirmar' element={<Confirmar />} />
        <Route path='*' element={<Pagina404 />} />
      </Routes>

    </>
  )
}
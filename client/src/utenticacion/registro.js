import React, { useEffect, useState } from "react";
import Select from 'react-select';
import axios from "axios";
export function Regitrate() {
  const [dato_ciudad, setciudad] = useState([])
  const [dato_entidad, setentidad] = useState([])
  const [valores, setvalores] = useState({
    nombre: '',
    apellido: '',
    ciudad: '',
    numero_documento: '',
    telefono: '',
    correo: '',
    direccion: '',
    tipo_usuario: '',
    entidad: '',
    contraseña: '',
    contraseñacon: ''


  })
  useEffect(() => {
    const obtenerciudad = async () => {
      const respon = await axios.get('http://localhost:3001/obtenerciudad')
      const resen = await axios.get('http://localhost:3001/obtenereps')
      setciudad(respon.data)
      setentidad(resen.data)

    }
    obtenerciudad()

  }, [valores])

  function cambioestado(e) {
    setvalores({ ...valores, [e.target.name]: e.target.value })

  }
  function enviardatos(e) {
    e.preventDefault()
    const registro = async () => {
      const respo = await axios.post('http://localhost:3001/registrar', valores)
      localStorage.setItem('token', respo.data.token)
      window.location.reload();

    }
    registro()


    setvalores({
      nombre: '',
      apellido: '',
      ciudad: '',
      numero_documento: '',
      telefono: '',
      correo: '',
      direccion: '',
      tipo_usuario: '',
      entidad: '',
      contraseña: '',
      contraseñacon: ''

    })
  }
  return (
    <div className="container p-4 mt-5">
      <div className="row">
        <div class="col-md-8 mx-auto conregis">
          <div class="card text-center">
            <div class="card-header">
              <h3>registrate</h3>
            </div>
            <div className="card-body containerre">
              <div>
                <img src='/logo_life shift.png' className="imagen" />
              </div>
              <form onSubmit={enviardatos}>
                <div className="d-flex flex-wrap gap-3  divform">
                  <input type="text" name='nombre' placeholder="nombre" className="inputregistro" onChange={cambioestado} required value={valores.nombre} />
                  <input type="text" name='apellido' placeholder="apellido" className="inputregistro" onChange={cambioestado} required value={valores.apellido} />
                  <select name='ciudad' className="inputregistro" onChange={cambioestado} value={valores.ciudad}>
                    <option value="">ciudad</option>
                    {dato_ciudad && Array.isArray(dato_ciudad) && dato_ciudad?.map((da, key) => {
                      return <option value={da.nombre_ciudad}>{da.nombre_ciudad}</option>
                    })}
                  </select>
                  <input type="text" name='numero_documento' placeholder="numero_documento" required pattern="\d*" className="inputregistro" onChange={cambioestado} value={valores.numero_documento} />

                  <input type="text" name='telefono' placeholder="telefono" className="inputregistro" onChange={cambioestado} value={valores.telefono} />
                  <input type='text' name='correo' placeholder="correo" className="inputregistro" onChange={cambioestado} value={valores.correo} />
                  <input type='text' name='direccion' placeholder="direccion" className="inputregistro" onChange={cambioestado} value={valores.direccion} />
                  <select className="inputregistro" name="tipo_usuario" onChange={cambioestado} value={valores.tipo_usuario}>
                    <option value=''>tipo de usuario</option>
                    <option value='contributibo'>contributibo</option>
                    <option value='subsidiado'>subsidiado</option>
                  </select>
                  <select className="inputregistro" name="entidad" onChange={cambioestado} value={valores.entidad}>
                    <option value=''>eps</option>
                    {dato_entidad && Array.isArray(dato_entidad) && dato_entidad?.map((da, key) => {
                      return <option value={da.nombre_entidad_prestadora}>{da.nombre_entidad_prestadora}</option>
                    })}

                  </select>
                  <input type='password' name='contraseña' placeholder="contraseña" className="inputregistro" onChange={cambioestado} value={valores.contraseña} />
                  <input type='password' name='contraseñacon' placeholder="confirmar_contraseña" className="inputregistro" onChange={cambioestado} value={valores.contraseñacon} />
                </div>
                <div className="botonregistro">
                  <button role='submit' className="botonre">registrarse</button>
                </div>
              </form>

            </div>
          </div>
        </div>
      </div>
    </div>

  )
}
import React, { useEffect, useState } from "react";
import { MdOutlineClose } from "react-icons/md";
import axios from "axios";
export function Loguiar() {

    const [valores, setvalores] = useState({
        documento: '',
        contraseña: ''


    })


    const [mensajepro, setmensaje] = useState('')
    const [estadomen, setestdo] = useState(false)

    return (
        <div className="container p-4 mt-5">
            <div className="row">
                <div class="col-md-4 mx-auto conregis">
                    <div class="card text-center">
                        <div class="card-header">
                            {!estadomen ? <h3>registrate</h3> : <div className="divspanlog" style={{ position: 'relative' }}><span className="espanlog" style={{ marginTop: '10px' }}>{mensajepro}</span> <button className="botonlogcerrar" style={{ position: 'absolute', right: '0px' }} onClick={() => {
                                setestdo(false)
                            }}><MdOutlineClose /></button></div>}
                        </div>
                        <div className="card-body containerre">
                            <div>
                                <img src='/logo_life shift.png' className="imagen" />
                            </div>
                            <form onSubmit={(e) => {
                                e.preventDefault()
                                async function petilog() {
                                    try {
                                        const dad = await axios.post('http://localhost:3001/loguiarse', valores)

                                        localStorage.setItem('token', dad.data.token)
                                        window.location.reload()


                                    } catch (error) {

                                        setmensaje(error.response.data.mensajee)
                                        setestdo(true)
                                    }


                                }
                                petilog()

                                setvalores(
                                    {
                                        documento: '',
                                        contraseña: ''
                                    }
                                )


                            }}>
                                <div className="d-flex flex-wrap gap-3  divform">
                                    <input type="text" name='documento' placeholder="documento" className="inputregistrolog" required onChange={(e) => {
                                        setvalores({ ...valores, [e.target.name]: e.target.value })
                                    }} value={valores.documento} />
                                    <input type="password" name='contraseña' placeholder="contraseña" className="inputregistrolog" onChange={(e) => {
                                        setvalores({ ...valores, [e.target.name]: e.target.value })
                                    }} value={valores.contraseña} />




                                </div>
                                <div className="botonregistro">
                                    <button role='submit' className="botonre">ingresar</button>
                                </div>
                            </form>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}
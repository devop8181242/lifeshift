import { Navegacionlog } from "../otras paginas/navegacionloguiado"
import { Normal } from "../otras paginas/volveralanormalidad"
import { useSelector } from "react-redux"
import { useEffect, useState } from "react"
import { useNavigate } from "react-router-dom"
import axios from "axios"
export function Confirmar(params) {
    const na = useNavigate()
    const consul = useSelector(state => state.datos.cedula)
    const [formu, setfomu] = useState([])
    const [otrodato, setotro] = useState({})
    const [modal, setmodal] = useState(false)
    const [reclamado, setreclamado] = useState(false)
    const [medico, setmedico] = useState({})
    const [medicafecha, setmedicafecha] = useState([])
    useEffect(() => {
        const traer = async () => {
            const r = await axios.post('http://localhost:3001/actulizarestado', { cedula: consul })

            setfomu(r.data.dato)


        }
        traer()

    }, [reclamado])
    useEffect(() => {
        const ob = async () => {
            console.log(otrodato.forid_medico)

            const rf = await axios.post('http://localhost:3001/obtenermedico', { id: otrodato.forid_medico })
            const rf2 = await axios.post('http://localhost:3001/traermedicamentosporfecha', { fecha: otrodato.forfecha_formula })


            setmedico(rf.data.dato[0])
            setmedicafecha(rf2.data.dato)


        }
        ob()


    }, [otrodato, modal])

    useEffect(() => {
        console.log(medicafecha)
    }, [medicafecha])


    const formatFechaResetado = (timestamp) => {
        const date = new Date(timestamp);
        const options = { day: 'numeric', month: 'long', year: 'numeric', hour: 'numeric', minute: 'numeric', hour12: true };
        const formattedDate = date.toLocaleDateString('es-ES', options);

        return formattedDate.replace(/(\d{1,2}:\d{2})/, (match, time) => {
            if (time.startsWith('0')) {
                time = time.replace(/^0/, '12');
            }
            return time;
        }).replace(',', ' a las');
    };




    return (
        <div className="homecontaiener conconfi" >
            <Navegacionlog className='logconf' />


            <div onClick={Normal} className="confirhis ">
                {
                    !formu ? <div class='nopen'><span>no tienes formulas pendientes</span> <button>regresar</button></div> : formu.length === 0 ? <div className="nopen"><span>no tienes formulas pendientes</span> <button onClick={() => {
                        na('/usuariopendiente')
                    }}><span>regresar </span> <i>←</i></button></div> : <div className="sicon container">{formu.map((form, key) => {
                        return <div key={key}>
                            <div className="row mt-2 d-flex justify-content-center">
                                <div className='card col-6'>
                                    <div className="card-body d-flex  align-items-center justify-content-center flex-column ">
                                        <span> formula pendiente</span>
                                        <div className="w-100 d-flex justify-content-center gap-5 mt-2">
                                            <button className="btn btn-info p-1" style={{ width: '80px' }} onClick={() => {
                                                setotro(form)
                                                setmodal(true)
                                            }}>ver</button>
                                            <button className="btn btn-secondary p-1" style={{ width: '80px' }} onClick={() => {
                                                const tra = async () => {
                                                    const re = await axios.post('http://localhost:3001/actulizarmedica', { fecha: form.forfecha_formula })
                                                    setreclamado(true)
                                                    alert(re.data.mensaje)

                                                }
                                                tra()
                                            }}>confirmar</button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    })}</div>
                }
                {
                    modal ? <div className=" formulahoja" >

                        <div className="row conho  col-10">
                            <div className="card  ">
                                <div className="card-head d-flex justify-content-end   ">
                                    <button className="btn btn-group " onClick={() => {
                                        setmodal(false)
                                    }}>x</button>

                                </div>
                                <div className="card-body ">
                                    <div>
                                        <strong>fecha_resatado:</strong>
                                        <span>{formatFechaResetado(otrodato.forfecha_formula)}</span>
                                        <br />
                                        <strong>hospital:</strong>
                                        <span>{otrodato.nombre_hospital}</span>
                                    </div>
                                    <div>
                                        <br />

                                        <strong>motivo de la consulta:</strong>
                                        <span>{otrodato.descripcion}</span>

                                    </div>

                                    <div>
                                        <br />

                                        <strong>nombre paciente:</strong>
                                        <span>{otrodato.nombre_usuario}</span>
                                        <br />
                                        <strong>apellido:</strong>
                                        <span>{otrodato.apellido}</span>

                                    </div>

                                    <div>
                                        <br />

                                        <strong>medico que lo atendio:</strong>
                                        <span>{medico.nombre_usuario}</span>
                                        <br />
                                        <strong>apellido:</strong>
                                        <span>{medico.apellido}</span>

                                        <br />
                                        <strong>numero_documento_medico:</strong>
                                        <span>{medico.numero_documento}</span>

                                    </div>
                                    <br />
                                    <br />

                                    <div>
                                        <table class="table table-striped table-hover  text-center">
                                            <thead>
                                                <tr>
                                                    <th scope="col">registro</th>
                                                    <th scope="col">medicamento</th>
                                                    <th scope="col">cantidad_resetada</th>
                                                    <th scope="col">dosis_resetada</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                {
                                                    medicafecha.map((med, key) => {
                                                        return <tr>
                                                            <th scope="row">{key}</th>
                                                            <td>{med.medicamento}</td>
                                                            <td>{med.cantidad}</td>
                                                            <td>{med.dosis}</td>
                                                        </tr>
                                                    })
                                                }


                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> : <></>
                }
            </div>
        </div >
    )

}
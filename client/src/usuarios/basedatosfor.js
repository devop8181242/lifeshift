export class Basefor {

    constructor(nombre) {
        this.nombre = nombre
        this.productos = []
    }

    async abrirbasedatos() {
        return new Promise((resolve, reject) => {
            this.IDBRequest = indexedDB.open(this.nombre, 1);
            this.IDBRequest.addEventListener('upgradeneeded', () => {
                this.db = this.IDBRequest.result;

                // Crear un objeto de almacenamiento de objetos con una clave personalizada
                const objectStore = this.db.createObjectStore(this.nombre, { keyPath: 'expediente' });

                // Asegúrate de que la clave sea única
                objectStore.createIndex('indexexpediente', 'expediente', { unique: true });
            });

            this.IDBRequest.addEventListener('success', () => {
                this.db = this.IDBRequest.result;
                resolve();
            });

            this.IDBRequest.addEventListener('error', () => {
                reject(this.IDBRequest.error);
            });
        });
    }






    async insertar(expediente, independiente) {

        const exists = await this.verificarExpedienteExistente(expediente);
        if (exists) {
            alert('producto ya agregado')
            return;
        }
        const trasn = this.modoinsertar('readwrite', 'agregado')
        trasn.add({ expediente, formula: independiente })


    }

    async verificarExpedienteExistente(expediente) {
        return new Promise((resolve, reject) => {
            this.db = this.IDBRequest.result
            const transaction = this.db.transaction(this.nombre, 'readonly');
            const objectStore = transaction.objectStore(this.nombre);
            const index = objectStore.index('indexexpediente');
            const request = index.get(expediente);

            request.onsuccess = (event) => {
                const result = event.target.result;
                resolve(!!result);
            };

            request.onerror = (event) => {
                console.error('Error al verificar la existencia del expediente:', event.target.error);
                reject(event.target.error);
            };
        });
    }

    async buscar() {

        const pro = await this.otromodo('readonly')
        return pro





    }

    otromodo(mode) {
        return new Promise((resolve, reject) => {
            this.db = this.IDBRequest.result
            this.trasnsaction = this.db.transaction(this.nombre, mode)
            this.objects = this.trasnsaction.objectStore(this.nombre)
            this.cursor = this.objects.openCursor()

            this.cursor.addEventListener("success", () => {

                if (this.cursor.result) {

                    this.res = { key: this.cursor.result.key, medica: this.cursor.result.value }
                    this.productos = [...this.productos, this.res]
                    this.cursor.result.continue()

                } else resolve(this.productos)
            })

            this.cursor.addEventListener('error', () => {
                reject('no se pudo traer el dato')
            })
        })



    }




    actulizar(ob) {
        if (!ob.hasOwnProperty('expediente')) {
            console.error("Object does not have 'expediente' property:", ob);
            return;
        }
        const trasn = this.modoactulizar("readwrite", "actulizado")
        trasn.put(ob)
    }

    eliminar(ob) {

        if (!ob.hasOwnProperty('expediente')) {
            console.error("Object does not have 'expediente' property:", ob);
            return;
        }
        const trasn = this.modoactulizar("readwrite", "eliminado")
        trasn.delete(ob.expediente)


    }


    modoinsertar(mode, msg) {
        this.db = this.IDBRequest.result
        this.trasnsaction = this.db.transaction(this.nombre, mode)
        this.objects = this.trasnsaction.objectStore(this.nombre)
        this.trasnsaction.addEventListener('complete', () => {
            alert(msg)




        })



        return this.objects







    }

    modoactulizar(mode, msg) {
        this.db = this.IDBRequest.result
        this.trasnsaction = this.db.transaction(this.nombre, mode)
        this.objects = this.trasnsaction.objectStore(this.nombre)
        this.trasnsaction.addEventListener('complete', () => {
            console.log(msg)




        })

        return this.objects







    }

    eliminarbasedatos() {
        indexedDB.deleteDatabase(this.nombre);
    }


}









import React, { useLayoutEffect, useState, useEffect } from 'react';
import { Navegacionlog } from '../otras paginas/navegacionloguiado';
import { agregarcedula } from '../feactures/cedula';
import { useDispatch } from 'react-redux';
import { useNavigate } from 'react-router-dom';
export function Usuariopendiente(params) {
    const [usupen, setusupen] = useState('')
    const dispach = useDispatch()
    const nav = useNavigate()
    useEffect(() => {
        if (!localStorage.getItem('token')) {
            nav('/usuario')
        }


    }, [])


    return (
        <>
            <div className="homecontaiener">
                <Navegacionlog />
                <div className='confirpaciente'>
                    <span className='spanclassfo'>
                        numero documento del paciente
                    </span>
                    <input type='text' placeholder='numero documento paciente' className='inp' onChange={(e) => {
                        setusupen(e.target.value)
                    }} />


                    <button className='botonfor' onClick={() => {
                        dispach(agregarcedula(usupen))
                        nav('/confirmar')
                    }}>continuar</button>


                </div>
            </div>
        </>
    )




}
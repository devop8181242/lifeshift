import React, { useLayoutEffect, useState, useEffect } from 'react';
import { Navegacionlog } from '../otras paginas/navegacionloguiado';
import { useNavigate } from 'react-router-dom';
import { useContext } from 'react'
import { content } from '../contexto'
import { Basefor } from './basedatosfor';
import { Normal } from '../otras paginas/volveralanormalidad';
import Cookies from 'js-cookie';
import axios from 'axios';
export function Resetar() {
    const [dato, setdato] = useState([])
    const [db, setdb] = useState('')
    const [actualizado, setactu] = useState(false)
    const { usu } = useContext(content)
    const navi = useNavigate()

    useEffect(() => {
        if (usu.tipo) {
            if (usu.tipo !== 'medico') {
                navi('/usuario')

            }


        }
        if (!Cookies.get('cookieestado')) {
            navi('/formula')
        }
        const ini = async () => {


            try {
                const db = new Basefor('formula')
                await db.abrirbasedatos('formula')
                const r = await db.buscar()
                setdato(r)
                setdb(db)

            } catch (error) {
                console.log(error)

            }


        }
        ini()
        setactu(false)
    }, [actualizado])


    if (dato) {
        console.log(dato)
    }



    const { paciente, datpa, cambiarestpa, numerosetdo } = useContext(content)
    console.log('datospaciente', datpa)
    return <div className='hom2'>
        <div className='homecontaiener'>
            <Navegacionlog />
            <div className='containerre'>
                <div className='rese'>
                    <div className='card conforcard'>
                        <div className='forreseta' onClick={Normal}>
                            {
                                dato && Array.isArray(dato) ? dato.map((dat, key) => {
                                    return <div className=' container mt-3 formula0'>
                                        <div className='card' >
                                            <div className=' card-body divformumla'>
                                                <div className=' imgfor'>
                                                    <img src={'http://localhost:3001/dbimages/' + `${dat.medica.expediente}monkeywit.png`} className='imgchildfor' />

                                                    <div style={{ maxWidth: '90px', textAlign: 'center' }}>{dat.medica.formula.articulo.producto}</div>
                                                </div>
                                                <div className='contantidad'>

                                                    <div className='cantidad'>
                                                        <button className='botonform' onClick={() => {
                                                            dat.medica.formula.cantidad--

                                                            db.actulizar(dat.medica)
                                                            setactu(true)

                                                        }}>🞀</button><span style={{ textAlign: 'center' }}>{dat.medica.formula.cantidad}</span><button className='botonform' onClick={() => {
                                                            dat.medica.formula.cantidad++

                                                            db.actulizar(dat.medica)
                                                            setactu(true)

                                                        }}>🞂</button>
                                                    </div>



                                                    <span>cantidad</span>
                                                </div>
                                                <div className='contantidad'>
                                                    <div className='cantidad'>
                                                        <button className='botonform' onClick={() => {
                                                            dat.medica.formula.dosis--

                                                            db.actulizar(dat.medica)
                                                            setactu(true)

                                                        }}>🞀</button><span style={{ textAlign: 'center' }}>{dat.medica.formula.dosis}</span><button className='botonform' onClick={() => {
                                                            dat.medica.formula.dosis++

                                                            db.actulizar(dat.medica)
                                                            setactu(true)

                                                        }}>🞂</button>
                                                    </div>

                                                    <span>dosis</span>
                                                </div>

                                                <div className='contantidad'>
                                                    <div className='cantidad'>
                                                        <button className='botonform' onClick={() => {
                                                            dat.medica.formula.tiempo--

                                                            db.actulizar(dat.medica)
                                                            setactu(true)

                                                        }}>🞀</button><span style={{ textAlign: 'center' }}>{dat.medica.formula.tiempo}</span><button className='botonform' onClick={() => {
                                                            dat.medica.formula.tiempo++

                                                            db.actulizar(dat.medica)
                                                            setactu(true)

                                                        }}>🞂</button>
                                                    </div>

                                                    <span>frecuencia</span>
                                                </div>
                                                <button className='btn btn-danger' onClick={() => {
                                                    db.eliminar(dat.medica)
                                                    setactu(true)

                                                }}>eliminar</button>
                                            </div>

                                        </div>
                                    </div>
                                }) : <></>
                            }



                        </div>

                        <div className='direct2'>

                            <div className='anterior'>
                                <a className='enlacea'><button className='btn btn-secondary antefor2' onClick={() => {
                                    navi('/formula')
                                }}>anterior</button></a>
                            </div>
                            <div className='posterior'>
                                <button className='btn btn-secondary despuesfor2' onClick={() => {
                                    const usume = JSON.parse(Cookies.get('medicamentos'))
                                    const date = { for: dato, documen: usume.numerodocumento, documendoct: usu.usuario.numero_documento }
                                    const reset = async () => {
                                        const res = await axios.post('http://localhost:3001/resetar', date)
                                        alert(res.data.mensaje)
                                        Cookies.remove('cookieestado')
                                        Cookies.remove('medicamentos')
                                        db.eliminarbasedatos()
                                        window.location.reload()
                                    }
                                    reset()


                                }}>resetar</button>
                            </div>
                        </div>

                    </div>



                </div>
            </div>
        </div>
    </div>

}
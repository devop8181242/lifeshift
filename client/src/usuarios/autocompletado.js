import React, { useLayoutEffect, useState, useEffect } from 'react';
import axios from "axios";
import { useNavigate } from 'react-router-dom'
export function Auto({ informa, set }) {
    const [dat, setda] = useState([])
    const navi = useNavigate()
    useEffect(() => {
        const odtener = async () => {
            try {
                let reponse = await axios.get(`http://localhost:3001/busquedad?q=${informa.informacion}`)
                setda(reponse.data.respu)

            } catch (error) {
                console.log(error)

            }

        }
        odtener()



    }, [informa])

    return <div className={informa.informacion !== '' ? 'clasebusab' : 'clasebus'}>
        {
            dat && Array.isArray(dat) ? dat.map((data, key) => {
                return <span onClick={() => {
                    navi(`/formula?medicamento=${data.producto}`)
                    set('')
                }}>{data.producto}</span>
            }) : <></>
        }
    </div>
}
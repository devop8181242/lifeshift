import React, { useLayoutEffect, useState, useEffect } from 'react';
import { Navegacionlog } from '../otras paginas/navegacionloguiado';
import { FaSearch } from "react-icons/fa";
import { Auto } from './autocompletado';
import { useContext } from 'react'
import { content } from '../contexto'
import axios from 'axios'
import { useLocation, useNavigate } from 'react-router-dom'
import { Basefor } from './basedatosfor';
import Cookies from 'js-cookie';

import '../general.css'
import { IoMdVideocam } from 'react-icons/io';
export function Formula() {
  const naviga = useNavigate();
  const [estadico, setcoo] = useState(false)
  const location = useLocation();
  const queryParams = new URLSearchParams(location.search);
  const [ob, setob] = useState('')
  const [datame, setdatame] = useState([])
  const [imagen, setimagen] = useState([])
  const medicamento = queryParams.get('medicamento');
  const [informa, setinforma] = useState({
    informacion: ''
  })
  const [dato, setdato] = useState([])
  const [bool, setbol] = useState(false)
  const [cantidad, setcantidad] = useState([])
  const [isfixed, setfixed] = useState(false)
  const [gregado, setagre] = useState({
    articulo: '',
    imagen: '',
    cantidad: 1,
    dosis: '',
    tiempo: ''

  })

  const { usu, paciente, datpa, cambiarestpa, numerosetdo } = useContext(content)
  useEffect(() => {


    window.scrollTo(0, 0)


    const ini = async () => {
      const db = new Basefor('formula')
      await db.abrirbasedatos('formula')
      setob(db)
    }
    ini()


  }, [])



  useEffect(() => {
    if (paciente) {
      Cookies.set('cookieestado', paciente)


      Cookies.set('medicamentos', JSON.stringify(datpa))

    }

  }, [paciente])



  useEffect(() => {

    const odtener = async () => {
      try {
        let reponse = await axios.get(`http://localhost:3001/busqueda?medicamento=${medicamento}`)
        setdatame(reponse.data.respu)

      } catch (error) {
        console.log(error)

      }

    }
    odtener()

  }, [medicamento])



  function modificar(result) {
    setinforma({ ...informa, informacion: result })

  }


  useLayoutEffect(() => {
    if (usu.tipo) {

      if (usu.tipo !== 'medico') {
        naviga('/Usuario')
      }
    } else {
      if (!localStorage.getItem('token')) {
        naviga('/Usuario')
        window.location.reload()
      }
    }

    const handleScroll = () => {
      const punto = 80;
      if (window.scrollY >= punto && !isfixed) {
        setfixed(true)
      } else if (window.scrollY < punto && isfixed) {
        setfixed(false)

      }

    }
    const peti = async () => {
      const res = await axios.get('http://localhost:3001/imagenes')
      // console.log(res.data.imagered)
      setimagen(res.data.imagered)
      setdato(res.data.imagen)

    }

    peti()

    if (bool) {



      const obteneralgo = async () => {
        try {
          const respu = await axios.post('http://localhost:3001/obtenerlis', gregado)
          alert(respu.data.mensaje)

        } catch (error) {
          console.log(error)
        }


      }
      //obteneralgo()


    }


    setbol(false)
    window.addEventListener('scroll', handleScroll)



    return () => {
      window.removeEventListener('scroll', handleScroll)
    }
  }, [bool, isfixed, usu, estadico])



  function agregar(arti, image, ...res) {
    const [can, dosis, tiempo] = res
    const nu = document.querySelector(`.${can}`)
    const dos = document.querySelector(`.${dosis}`)
    const teim = document.querySelector(`.${tiempo}`)

    const valores = {
      articulo: arti,
      imagen: image,
      [nu.name]: parseInt(nu.value),
      [dos.name]: parseInt(dos.value),
      [teim.name]: parseInt(teim.value)
    }

    ob.insertar(arti.expediente, valores)
    setagre((prevEstado) => {
      const nuevoEstado = { ...prevEstado, articulo: arti, imagen: image, [nu.name]: nu.value, [dos.name]: parseInt(dos.value), [teim.name]: parseInt(teim.value) };

      return nuevoEstado;
    });

    nu.value = ''
    dos.value = ''
    teim.value = ''





    setbol(true)












    //setagre({...gregado,[nu.name]:nu.value})

    /*if(gregado.arti!=='' && gregado.imagen!==''){
    const obteneralgo=async ()=>{
        try {
            const respu=await axios.post('http://localhost:3001/obtenerlis', gregado)
           alert(respu.data.mensaje)

         } catch (error) {
            console.log(error)
         }
    }
    obteneralgo()
    } */

  }

  /*function obtenercan(da){
   async function obtenerar() {
     const res=await axios.get('http://localhost:3001/obtenerarti')
     console.log(res.data[0].cantidad)
     setcantidad()
   }
   
  
   obtenerar()
  }  */

  function definirtex(e) {
    e.target.style.height = 'auto'; // Restablece la altura a auto para que pueda expandirse
    e.target.style.height = e.target.scrollHeight + 'px'; // Establece la altura según el contenido
    if (e.target.scrollHeight >= 250) {
      e.target.style.overflowY = 'scroll';
    }


  }


  //console.log(JSON.parse(Cookies.get('medicamentos')))
  if (usu.tipo === 'medico') {
    if (Cookies.get('cookieestado') === 'true') {
      if (medicamento !== null && medicamento !== "") {
        return (

          <>

            <div className="homecontaiener">
              <div className='child2'>
                <Navegacionlog />
              </div>
              <div className='conbuscamedicamento'>
                <div className={isfixed ? 'conchildbuscamedicamentofixed' : 'conchildbuscamedicamento'}>
                  <div className='conbus'>
                    <input type='text' className='buscamedicamento' name='informacion' onChange={(e) => {
                      setinforma({ ...informa, [e.target.name]: e.target.value })

                    }} value={informa.informacion} />
                    <Auto informa={informa} set={modificar} />
                    <div className='iconsearchcon'>
                      <FaSearch className='iconsearch' onClick={() => {
                        naviga(`/formula?medicamento=${informa.informacion}`)
                        modificar('')
                      }} />
                    </div>
                  </div>
                </div>
                <div className='child4'>
                  {
                    datame && Array.isArray(datame) && datame?.map((da, key) => {
                      return <div className='card img'>
                        <div className='card-body d-flex flex-column justify-content-around  align-items-center contaima'>
                          <img className='imad' key={key} src={'http://localhost:3001/dbimages/' + `${da.expediente}monkeywit.png`} /> <span>{da.producto}</span> <input className={`inputmedicamentocantidad${key}`} name="cantidad" style={{ width: '50px', textAlign: 'center' }} /><span>cantidad</span> <input className={`inputmedicamentodosis${key}`} name="dosis" style={{ width: '50px', textAlign: 'center' }} /><span>dosis</span>  <input className={`inputmedicamentotiempo${key}`} name="tiempo" style={{ width: '50px', textAlign: 'center' }} /><span>tiempo_consumo</span>  <button className='btn btn-success w-100' onClick={() => agregar(da, `${da.expediente}monkeywit.png`, `inputmedicamentocantidad${key}`, `inputmedicamentodosis${key}`, `inputmedicamentotiempo${key}`)}>agregar</button>
                        </div>
                      </div>


                    })
                  }



                </div>
                <div className='direct'>

                  <div className='anterior'>
                    <a className='enlacea'><button className='btn btn-close-white antefor'>anterior</button></a>
                  </div>
                  <div className='posterior'>
                    <a href='/resetar' className='enlacea' ><button className='btn btn-secondary despuesfor'>siguiente</button></a>
                  </div>
                </div>
              </div>

            </div>
          </>
        );
      } else {
        return (

          <>

            <div className="homecontaiener">
              <div className='child2'>
                <Navegacionlog />
              </div>
              <div className='conbuscamedicamento'>
                <div className={isfixed ? 'conchildbuscamedicamentofixed' : 'conchildbuscamedicamento'}>
                  <div className='conbus'>
                    <input type='text' className='buscamedicamento' name='informacion' onChange={(e) => {
                      setinforma({ ...informa, [e.target.name]: e.target.value })

                    }} value={informa.informacion} />
                    <Auto informa={informa} set={modificar} />
                    <div className='iconsearchcon'>
                      <FaSearch className='iconsearch' />
                    </div>
                  </div>
                </div>
                <div className='child3'>
                  {
                    dato && Array.isArray(dato) && dato?.map((da, key) => {
                      return <div className='card img'>
                        <div className='card-body d-flex flex-column justify-content-around  align-items-center contaima'>
                          <img className='imad' key={key} src={'http://localhost:3001/dbimages/' + `${da.expediente}monkeywit.png`} /> <span>{da.producto}</span> <input className={`inputmedicamentocantidad${key}`} name="cantidad" style={{ width: '50px', textAlign: 'center' }} /><span>cantidad</span>  <input className={`inputmedicamentodosis${key}`} name="dosis" style={{ width: '50px', textAlign: 'center' }} /><span>dosis</span>  <input className={`inputmedicamentotiempo${key}`} name="tiempo" style={{ width: '50px', textAlign: 'center' }} /><span>tiempo_consumo</span>  <button className='btn btn-success w-100' onClick={() => agregar(da, `${da.expediente}monkeywit.png`, `inputmedicamentocantidad${key}`, `inputmedicamentodosis${key}`, `inputmedicamentotiempo${key}`)}>agregar</button>

                        </div>
                      </div>


                    })
                  }



                </div>

                <div className='direct'>

                  <div className='anterior'>
                    <a className='enlacea'><button className='btn btn-close-white antefor'>anterior</button></a>
                  </div>
                  <div className='posterior'>
                    <a href='/resetar' className='enlacea' ><button className='btn btn-secondary despuesfor'>siguiente</button></a>
                  </div>
                </div>
              </div>

            </div>
          </>
        );
      }
    } else {
      return (
        <>
          <div className='homecontaiener'>

            <Navegacionlog />
            <div className='confirpaciente'>
              <span className='spanclassfo'>
                numero documento del paciente
              </span>
              <input type='text' placeholder='numero documento paciente' className='inp' onChange={(e) => {
                cambiarestpa({ ...datpa, numerodocumento: e.target.value })

              }} value={datpa.numerodocumento} />
              <span className='spanclassfo'>
                nombre
              </span>
              <input type='text' placeholder='nombre' className='inp' onChange={(e) => {
                cambiarestpa({ ...datpa, nombre: e.target.value })

              }} value={datpa.nombre} />
              <span className='spanclassfo'>
                apellido
              </span>
              <input type='text' placeholder='apellido' className='inp' onChange={(e) => {
                cambiarestpa({ ...datpa, apellido: e.target.value })

              }} value={datpa.apellido} />
              <span className='spanclassfo'>
                motivo de consulta
              </span>
              <textarea
                onInput={definirtex}
                onKeyDown={(e) => {
                  // Verificar si se ha presionado la tecla de tabulación
                  if (e.key === "Tab") {
                    e.preventDefault(); // Prevenir el comportamiento predeterminado de la tecla de tabulación
                    const start = e.target.selectionStart;
                    const end = e.target.selectionEnd;

                    // Insertar un tabulador en la posición actual del cursor
                    e.target.value = e.target.value.substring(0, start) + '\t' + e.target.value.substring(end);

                    // Establecer la posición del cursor después del tabulador insertado
                    e.target.selectionStart = e.target.selectionEnd = start + 1;

                    // Ajustar la altura del textarea

                  }
                }}

                onChange={(e) => {
                  cambiarestpa({ ...datpa, motivo: e.target.value })


                  console.log(datpa)
                }} className='text' value={datpa.motivo} ></textarea>

              <button className='botonfor' onClick={() => {
                const odtenerpa = async () => {
                  try {
                    let reponse = await axios.get(`http://localhost:3001/pacienteexiste?paciente=${JSON.stringify(datpa)}`)
                    numerosetdo(true)

                    window.location.reload()


                  } catch (error) {
                    alert(error.response.data.mensaje)
                  }

                }
                odtenerpa()
              }}>continuar</button>


            </div>
          </div>
        </>
      )
    }
  } else {
    return <>
    </>
  }


}
import React, { useEffect, useState, useContext, useRef } from 'react';
import { useNavigate } from 'react-router-dom'
import { useLocation } from 'react-router-dom'
import { MdOutlineArrowDropDown } from "react-icons/md";

import axios from 'axios'
import { FaBars } from "react-icons/fa6";
import { FaUserLarge } from "react-icons/fa6";
import { MdOutlineAttachment } from "react-icons/md";
import { content } from "../contexto"
import { Estado } from '../verificacion/estado';
export function Informacion({ navigate }) {
  const fileInputRef = useRef(null);
  const location = useLocation();
  const queryParams = new URLSearchParams(location.search);
  const [estado, setestado] = useState(false)
  const [estado2, setestado2] = useState(false)
  const [usuariomo, setusuariomo] = useState(false)
  const [foto, setfoto] = useState({
    foto: ''
  })
  const [ciudades, setciudad] = useState([])
  const [epsa, seteps] = useState([])
  const documento = queryParams.get('documento');
  const navegacion = navigate();
  const { usu } = useContext(content)
  const [datosus, setdatous] = useState({
    numero_documento: '',
    nombre: '',
    apellido: '',
    ciudad: '',
    eps: '',
    direccion: '',
    numerotelefonico: '',
    tipo_usuario: '',
    foto: '',





  })
  useEffect(() => {

    if (usu.usuario.nombre_usuario) {

      console.log(usu)
      const datou = async () => {

        try {
          const responciueps = await axios.post("http://localhost:3001/ciudadus", usu.usuario)
          const respon = await axios.get('http://localhost:3001/obtenerciudad')
          const resen = await axios.get('http://localhost:3001/obtenereps')

          setciudad(respon.data)
          seteps(resen.data)
          setdatous({ ...datosus, numero_documento: usu.usuario.numero_documento, nombre: usu.usuario.nombre_usuario, apellido: usu.usuario.apellido, direccion: usu.usuario.direccion, numerotelefonico: usu.usuario.numerotelefonico, ciudad: responciueps.data.ciudad.nombre_ciudad, eps: responciueps.data.eps.nombre_entidad_prestadora, tipo_usuario: usu.usuario.tipo_usuario })

        } catch (error) {
          console.log('error')

        }



        /*setdatous({
         nombre:usu.nombre_usuario,
         apellido:usu.apellido
        })  */




      }

      datou()

    } else {
      if (!localStorage.getItem('token') && usu.usuario.confirmar === 0) {
        navegacion('/')
      }



    }



  }, [usu])
  useEffect(() => {
    //console.log(estado)


  }, [estado, estado2, datosus])



  function manejarcambios(e) {
    setdatous({ ...datosus, [e.target.name]: e.target.value })
  }

  const handleFileButtonClick = () => {
    fileInputRef.current.click();
  };

  const handleFileChange = (event) => {

    const reader = new FileReader()
    reader.readAsDataURL(event.target.files[0])
    reader.addEventListener("load", () => {

      let url = URL.createObjectURL(event.target.files[0])
      console.log(url)
      setdatous({ ...datosus, foto: url })
      setfoto({ ...foto, foto: event.target.files[0] })

    })

  };

  const cargarfotos = async () => {
    const formData = new FormData();
    formData.append('file', foto.foto);

    await axios.post('http://localhost:3001/cargarfotoperfil', formData, {
      headers: {

        'Content-Type': 'multipart/form-data',
      },
      params: {
        documento: usu.usuario.numero_documento,
      },
    });

  }
  const actulizar_datos = async () => {
    const res = await axios.post('http://localhost:3001/modificarusuario', { datou: datosus })
    alert(res.data.men)
  }

  const manejarcan = (e) => {
    setdatous({ ...datosus, [e.target.name]: e.target.value })
  }

  return <>
    <div >
      <form className='forminfor' onSubmit={async (e) => {
        e.preventDefault()

        await cargarfotos()
        await actulizar_datos()


        window.location.reload()

      }}>
        <div className='containerinforma'>
          <div className='forminfo'>
            <div>
              <label className='labeldes'>nombre</label>
              <input type="text" className='inputinfo' placeholder='nombre' name="nombre" value={datosus.nombre} onChange={manejarcambios} />
            </div>
            <div>
              <label className='labeldes'>apellido</label>
              <input type="text" className='inputinfo' placeholder='apellido' name='apellido' value={datosus.apellido} onChange={manejarcambios} />
            </div>
            <div className='containerdes'>
              <label className='labeldes'>ciudad</label>
              <input type="text" className='inputinfo' placeholder='ciudad' name='ciudad' value={datosus.ciudad} onChange={manejarcambios} />
              < MdOutlineArrowDropDown className='downdesple' onClick={() => {
                if (estado === false) {
                  const fragmento = document.createDocumentFragment()
                  ciudades.forEach((ci, key) => {
                    const elenew = document.createElement('span')
                    elenew.classList.add('eleciudaddes')
                    elenew.textContent = ci.nombre_ciudad

                    elenew.addEventListener('click', () => {
                      setdatous({ ...datosus, ciudad: ci.nombre_ciudad })
                      setestado(false)
                      const ele = document.querySelector('.containerdes')
                      const newele = document.querySelector('.childes')

                      ele.removeChild(newele)


                    })

                    fragmento.appendChild(elenew)
                  })
                  const ele = document.querySelector('.containerdes')
                  const newele = document.createElement('div')
                  newele.classList.add('childes')
                  newele.appendChild(fragmento)
                  ele.appendChild(newele)


                } else {
                  const ele = document.querySelector('.containerdes')
                  const newele = document.querySelector('.childes')

                  ele.removeChild(newele)

                }

                setestado(!estado)


              }} />
            </div>
            <div className='containerdes2'>
              <label className='labeldes'>eps</label>
              <input type="text" className='inputinfo' placeholder='eps' name='eps' value={datosus.eps} onChange={manejarcambios} />
              < MdOutlineArrowDropDown className='downdesple' onClick={() => {
                if (!estado2) {
                  const fragmento = document.createDocumentFragment()
                  epsa?.forEach((ep, key) => {
                    const elenew2 = document.createElement('span')
                    elenew2.classList.add('eleciudaddes')
                    elenew2.textContent = ep.nombre_entidad_prestadora

                    elenew2.addEventListener('click', () => {
                      setdatous({ ...datosus, eps: ep.nombre_entidad_prestadora })
                      setestado2(false)
                      const ele = document.querySelector('.containerdes')
                      const newele2 = document.querySelector('.childes2')
                      ele.removeChild(newele2)


                    })
                    fragmento.appendChild(elenew2)
                  })
                  const ele2 = document.querySelector('.containerdes')
                  const newele2 = document.createElement('div')
                  newele2.classList.add('childes2')
                  newele2.appendChild(fragmento)
                  ele2.appendChild(newele2)
                } else {
                  const ele = document.querySelector('.containerdes')
                  const newele2 = document.querySelector('.childes2')

                  ele.removeChild(newele2)

                }
                setestado2(!estado2)

              }} />
            </div>

            <div>
              <label className='labeldes'>direccion</label>
              <input type="text" className='inputinfo' placeholder='direccion' name='direccion' value={datosus.direccion} onChange={manejarcambios} />
            </div>
            <div>
              <label className='labeldes'>telefono</label>
              <input type="text" className='inputinfo' placeholder='direccion' name='numerotelefonico' value={datosus.numerotelefonico} onChange={manejarcambios} />
            </div>
            <div>
              <label className='labeldes'>tipo_usuario</label>
              {datosus.tipo_usuario === 'subsidiado' ? <select className="inputinfo" name="tipo_usuario" onChange={manejarcan}>
                <option value='subsidiado'>subsidiado</option>
                <option value='contributibo'>contributibo</option>

              </select> : <select className="inputinfo" name="tipo_usuario" onChange={manejarcan}>
                <option value='contributibo'>contributibo</option>
                <option value='subsidiado'>subsidiado</option>

              </select>}

            </div>

          </div>

          <div className='conimginfogrid'>
            <div className='conimginfo'>
              {datosus.foto !== '' ? <img src={datosus.foto} className='iconimginfo' /> : usu.usuario.foto_perfil ? <img src={`http://localhost:3001/images/${usu.usuario.foto_perfil}-imagenserver-.png`} className='iconimginfo' /> : <FaUserLarge className='iconimginfo' />}
            </div>
            <div id='iconsttach'>

              {/* Hidden file input */}
              <input
                type="file"
                ref={fileInputRef}
                style={{ display: 'none' }}
                onChange={handleFileChange}
              />

              <MdOutlineAttachment title="cargar imagen" className='cargarimg' onClick={handleFileButtonClick} />
            </div>

          </div>

        </div>
        <div className='botoninfogua'>
          <input type='submit' value="guardar" />

        </div>

      </form>

    </div>

  </>
}
import { Navegacionlog } from "../otras paginas/navegacionloguiado"
import { Normal } from "../otras paginas/volveralanormalidad"
import { IoMdLogOut } from "react-icons/io"
import { FaBars } from "react-icons/fa6";
import { FaKitMedical } from "react-icons/fa6";
import { Link } from "react-router-dom";
import { content } from "../contexto"
import { FaUserDoctor } from "react-icons/fa6";
import { useNavigate } from "react-router-dom";
import { FaUserLarge } from "react-icons/fa6";
export function Farmaseutico({ usu }) {
    return (
        <>
            <div className="conusuario2 "  >
                <Navegacionlog />



                <div className="conchild" onClick={Normal}>
                    <div>
                        <div className="divimgusu">
                            {usu ? <span>{`${usu.usuario.nombre_usuario} ${usu.usuario.apellido} `}</span> : <span>castillo</span>}
                            {usu.usuario.foto_perfil ? <div className="condivusufo">
                                <img src={`http://localhost:3001/images/${usu.usuario.foto_perfil}-imagenserver-.png`} className="imguser" />
                            </div> : <div className="condivusufo">
                                <FaUserLarge />
                            </div>}
                        </div>
                        <div className="divimgusu divusuchild">
                            <ul className="ul" >


                                <li style={{ listStyle: 'none' }}>
                                    <a href={`/informacion?documento=${usu.usuario.numero_documento}`} style={{ textDecoration: 'none', color: 'black' }}>
                                        <FaBars />
                                        <span >informacion</span>
                                    </a>
                                </li>

                                <li style={{ listStyle: 'none' }}>
                                    <a href='/historialmedicamento' style={{ textDecoration: 'none', color: 'black' }}>
                                        <FaKitMedical />
                                        <span >medicamentos</span>
                                    </a>
                                </li>
                                <li style={{ listStyle: 'none' }}>
                                    <Link to={`/usuariopendiente`} style={{ textDecoration: 'none', color: 'black' }}>
                                        <FaUserDoctor />
                                        <span >entregar</span>
                                    </Link>
                                </li>
                                <li style={{ listStyle: 'none' }}>

                                    <IoMdLogOut />
                                    <span onClick={() => {
                                        localStorage.removeItem('token')
                                        window.location.reload()
                                    }}>salir</span>
                                </li>






                            </ul>
                        </div>
                    </div>


                </div>


            </div>
        </>
    )
}
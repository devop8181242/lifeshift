import { Navegacionlog } from "../otras paginas/navegacionloguiado"
import { FaUserLarge } from "react-icons/fa6";
import React, { useEffect, useState, useContext } from 'react';
import { IoMdLogOut } from "react-icons/io"
import { FaBars } from "react-icons/fa6";
import { FaKitMedical } from "react-icons/fa6";
import { content } from "../contexto"
import { FaUserDoctor } from "react-icons/fa6";
import { Normal } from "../otras paginas/volveralanormalidad";
import { useNavigate } from "react-router-dom";
import { Farmaseutico } from "./farmaseutico";
export function Usuariocomun() {
  const { usu } = useContext(content)
  const navi = useNavigate()
  useEffect(() => {

    if (usu.usuario) {
      if (usu.usuario.confirmar.data[0] === 0) {
        navi('/verificacion')
      }
    }


  }, [usu])

  if (usu.tipo === 'comun') {
    return (
      <>
        <div className="conusuario2 "  >
          <Navegacionlog />



          <div className="conchild" onClick={Normal}>
            <div>
              <div className="divimgusu">
                {usu ? <span>{`${usu.usuario.nombre_usuario} ${usu.usuario.apellido} `}</span> : <span>castillo</span>}
                {usu.usuario.foto_perfil ? <div className="condivusufo">
                  <img src={`http://localhost:3001/images/${usu.usuario.foto_perfil}-imagenserver-.png`} className="imguser" />
                </div> : <div className="condivusufo">
                  <FaUserLarge />
                </div>}
              </div>
              <div className="divimgusu divusuchild">
                <ul className="ul" >


                  <li style={{ listStyle: 'none' }}>
                    <a href={`/informacion?documento=${usu.usuario.numero_documento}`} style={{ textDecoration: 'none', color: 'black' }}>
                      <FaBars />
                      <span >informacion</span>
                    </a>
                  </li>

                  <li style={{ listStyle: 'none' }}>
                    <a href='/historialmedicamento' style={{ textDecoration: 'none', color: 'black' }}>
                      <FaKitMedical />
                      <span >medicamentos</span>
                    </a>
                  </li>
                  <li style={{ listStyle: 'none' }}>

                    <IoMdLogOut />
                    <span onClick={() => {
                      localStorage.removeItem('token')
                      window.location.reload()
                    }}>salir</span>
                  </li>




                </ul>
              </div>
            </div>


          </div>


        </div>
      </>
    )

  } else {

    if (usu.tipo === 'farmaceutico') {
      return (<Farmaseutico usu={usu} />)

    } else {
      return (
        <>
          <div className="conusuario2 " >
            <Navegacionlog />



            <div className="conchild" onClick={Normal}>
              <div className="usuariolog7">
                <div className="divimgusu">
                  {usu ? <span>{`${usu.usuario.nombre_usuario} ${usu.usuario.apellido} `}</span> : <span>castillo</span>}
                  <div className="condivusufo">
                    {usu.usuario.foto_perfil ? <div className="condivusufo">
                      <img src={`http://localhost:3001/images/${usu.usuario.foto_perfil}-imagenserver-.png`} className="imguser" />
                    </div> : <div className="condivusufo">
                      <FaUserLarge />
                    </div>}
                  </div>
                </div>
                <div className="divimgusu divusuchild">
                  <ul className="ul" >


                    <li style={{ listStyle: 'none' }}>
                      <a href={`/informacion?documento=${usu.usuario.numero_documento}`} style={{ textDecoration: 'none', color: 'black' }}>
                        <FaBars />
                        <span >informacion</span>
                      </a>
                    </li>

                    <li style={{ listStyle: 'none' }}>
                      <a href='#!' style={{ textDecoration: 'none', color: 'black' }}>
                        <FaKitMedical />
                        <span >medicamentos</span>
                      </a>
                    </li>

                    <li style={{ listStyle: 'none' }} className="forusu">
                      <a href="/formula" style={{ textDecoration: 'none', color: 'black' }}>
                        <FaUserDoctor />
                        <span >formular</span>
                      </a>
                    </li>
                    <li style={{ listStyle: 'none' }} >

                      <IoMdLogOut />
                      <span onClick={() => {
                        localStorage.removeItem('token')
                        window.location.reload()
                      }}>salir</span>
                    </li>





                  </ul>
                </div>
              </div>


            </div>


          </div>
        </>
      )
    }


  }
}

const { promisify } = require('util')
const mysql = require('mysql2')
const pool = mysql.createPool({
    host: 'localhost',//process.env.MYSQL_HOST || 'host.docker.internal',  // Usa 'localhost' si no estás en Docker
    //port: process.env.MYSQL_PORT ||  3306,
    user: 'root', //process.env. MYSQL_USER || 'root',
    password: 'root125', //process.env.MYSQL_ROOT_PASSWORD || 'root125',
    database: 'lifeshift'
})

pool.getConnection((err, connection) => {
    if (err.code === 'PROTOCOL_CONECTION_LOST') {
        console.log('conection a la base de datos cerrada')
    }
    if (err.code === 'ER_CON_COUNT_ERROR') {
        console.log('BASE DE DATOS TIENE MUCHAS CONECTIONES')//para comprobar cuantas conectiones  tiene mi base de datos has el momen
    }
    if (err.code === 'ECONNREFUSED') {
        console.log('CONEXION FUE RECHASADA')//para comprobar cuantas conectiones  tiene mi base de datos has el momen
    }
    if (connection) {
        connection.release()//con esto empisa la connection
    }
    console.log('base de datos conectada')
    return;
})
pool.query = promisify(pool.query)//esto me permite que cada ves que haga una consulta sea con promesas  para resumirlo estamos convirtiendo a promesas lo que antes era en callback
module.exports = pool;
const jwt = require('jsonwebtoken');
const verificarToken = (req, res, next) => {
     // Obtén el token del encabezado de la solicitud
    const token = req.headers.authorization;
  
     // Verificar si el token está presente
    if (!token) {
        console.log(token)
      return res.status(401).json({ mensaje: 'Token no proporcionado' });
    }
  
    try {
      // Verificar y descodificar el token
      const decoded = jwt.verify(token.replace('Bearer ', ''), 'secret_token');
      
      // Agregar la información del usuario decodificada al objeto de solicitud
      req.usuario = decoded;
  
      // Continuar con el siguiente middleware o ruta
      next();
    } catch (error) {
        console.log('res')
        return res.status(401).json({ mensaje: 'Token no válido' });
  
      }
    };
    module.exports={
        verificarToken
    }
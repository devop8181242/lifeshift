//las clases abstractas solo definen las reglas
const { google } = require('googleapis')
const nodemailer = require('nodemailer');
const oauth2 = google.auth.OAuth2
const accountTransport = require("../acount_transpor.json");
let oauth2Client;
// creamos nuestro transpor directamente estomos creando una referencia anuestro accountransport
const setupoauth2Client = () => {

    oauth2Client = new oauth2(
        accountTransport.auth.clientId,
        accountTransport.auth.clientSecret,
        "https://developers.google.com/oauthplayground",
    );
    oauth2Client.setCredentials({
        refresh_token: accountTransport.auth.refreshToken,
        tls: {
            rejectUnauthorized: false
        }
    });
    // me refresca el token cada ves que envio un nuevo imail

    oauth2Client.getAccessToken((err, token) => {
        if (err) {
            return console.log("error");
        }
        accountTransport.auth.accessToken = token;
        //retorna este objeto que nos va permitir enviar el gmail

    });



};
// Call setupOAuth2Client to initialize oauth2Client

setupoauth2Client()
// Define your mail transporter using the global oauth2Client
const transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
        type: 'OAuth2',
        user: 'luisdacade@gmail.com',
        clientId: accountTransport.auth.clientId,
        clientSecret: accountTransport.auth.clientSecret,
        refreshToken: accountTransport.auth.refreshToken,
        accessToken: oauth2Client.getAccessToken(),
    },
});

// Now you can use the transporter in various methods
const sendEmail = (mailOptions) => {

    transporter.sendMail(mailOptions, (error, info) => {
        if (error) {
            console.error('esto es un error', error);
        } else {
            console.log('Email sent:', info.response);
        }
    });
};

class Codigoverificion {
    constructor() {
        if (new.target === Codigoverificion) {
            throw new Error('no puede instanciar una clase clase abstracta')
        }
    }

    enviarcodigo() {
        throw new Error('debe ser implementado por sus clases abstractas')
    }
}
class Codigoverificioncorreogmail extends Codigoverificion {
    correo
    codigo
    constructor(correo, codigo) {
        super()
        this.correo = correo
        this.codigo = codigo

    }
    enviarcodigo() {
        // Contenido del correo electrónico con HTML y estilos
        const htmlContent = `

  
<div style="max-width: 600px; margin: 0 auto; padding: 20px; background-color: #fff; border-radius: 5px; box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);">
<h1 style="color: #007bff;">${this.codigo.toString()}</h1>
<span>codigo de verificacion</span>
</div>
 
`;
        const mailOptions = {
            from: 'luisdacade@gmail.com',
            to: `${this.correo}`,
            subject: 'codigo de verificacion de life_shift',
            html: htmlContent,
        };
        sendEmail(mailOptions)
    }
    set condigover(codigo) {
        this.codigo = codigo

    }
    get condigover() {
        return this.codigo
    }
}

class Codigoverificioncorreohotmail extends Codigoverificion {
    correo
    codigo
    constructor(correo, codigo) {
        super()
        this.correo = correo
        this.codigo = codigo

    }
    enviarcodigo() {

    }
}
module.exports = {
    Codigoverificioncorreogmail
}
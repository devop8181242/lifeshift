const { Router } = require('express')
const { promisify } = require('util')
const mysql = require('mysql2')
const pool = require('../db')
const jwt = require('jsonwebtoken');
const { verificarToken } = require('./verificar')
const { Codigoverificioncorreogmail } = require('./metodoverificacion')
const rauteclien = Router()
rauteclien.post('/registrar', async (req, res) => {
  const codigo = Math.floor(1000 + Math.random() * 9000);
  let { nombre, apellido, ciudad, numero_documento, telefono, correo, direccion, tipo_usuario, entidad, contraseña, contraseñacon } = req.body
  const id_ciudad = await pool.query('select id_ciudad from ciudades where nombre_ciudad=?', [ciudad])
  const id_entidad = await pool.query('select id_entidad from entidad_prestadora where nombre_entidad_prestadora=?', [entidad])
  if (id_ciudad && id_entidad && codigo) {
    const insert = await pool.query('insert into usuarios  set ?', [{ numero_documento, contraseña, nombre_usuario: nombre, apellido, id_ciudad_usuario: id_ciudad[0].id_ciudad, direccion, correo, numerotelefonico: telefono, id_entidad_usuario: id_entidad[0].id_entidad, tipo_usuario, codigo }])
    await pool.query(`CREATE USER '${numero_documento}' IDENTIFIED BY '${contraseña}'`)

    await pool.query(' GRANT SELECT ON lifeshift.* TO ? ', [numero_documento])

    await pool.query('  GRANT UPDATE ON lifeshift.usuarios TO ?', [numero_documento])
    await pool.query('FLUSH PRIVILEGES');

    if (insert) {
      try {
        let codi = new Codigoverificioncorreogmail(correo, codigo)
        codi.enviarcodigo()
      } catch (error) {
        console.log('error')
      }


    }



  }

  const result = await pool.query(`select * from usuarios where numero_documento=?`, [numero_documento])
  if (result.length > 0) {
    re = result[0]

    try {
      const token = jwt.sign({ re }, 'secret_token');
      return res.status(200).json({ token });
    } catch (error) {
      console.error('Error signing JWT:', error);
      return res.status(500).json({ error: 'Internal Server Error' });
    }
  }


})

rauteclien.post('/loguiarse', async (req, res) => {

  try {
    const { documento, contraseña } = req.body;
    console.log(contraseña)
    result = await pool.query('select * from usuarios where numero_documento=?', [documento])
    if (result.length > 0) {
      re = result[0]
      if (contraseña === re.contraseña) {

        try {
          const token = jwt.sign({ re }, 'secret_token');
          return res.status(200).json({ token });
        } catch (error) {
          console.error('Error signing JWT:', error);
          return res.status(500).json({ mensajee: 'Internal Server Error' });
        }


      } else {

        return res.status(500).json({ mensajee: 'contraseña incorrecta' });
      }

    } else {
      console.log(result)
      console.group(documento)
      return res.status(500).json({ mensajee: 'usuario no reguistrado' });
    }
  } catch (error) {
    console.log(error)
    return res.status(500).json({ mensajee: 'nuestra aplicacion esta fallando', error });
  }

  // Aquí puedes realizar acciones con la información del usuario
  // ...

  //res.json({ mensaje: 'Datos', dato:re2[0]});  
})


rauteclien.get('/logiado', verificarToken, async (req, res) => {
  const usuario = req.usuario;


  re2 = await pool.query('select * from usuarios where numero_documento=?', [usuario.re.numero_documento])

  medico = await pool.query('select * from medicos where id_usuario_medico=?', [usuario.re.numero_documento])

  farma = await pool.query('select * from farmaceutico where id_usuario_farmaceutico=?', [usuario.re.numero_documento])
  if (medico.length > 0) {
    res.json({ mensaje: 'Datos', dato: re2[0], tipo: 'medico' });
  } else if (farma.length > 0) {
    res.json({ mensaje: 'Datos', dato: re2[0], tipo: 'farmaceutico' });

  } else {
    res.json({ mensaje: 'Datos', dato: re2[0], tipo: 'comun' });
  }
  // Aquí puedes realizar acciones con la información del usuario
  // ...



})

rauteclien.post('/modificarestado', async (req, res) => {
  const { usu } = req.body

  try {

    await pool.query('update usuarios set confirmar=? where numero_documento=?', [1, usu.usuario.numero_documento])
    console.log('actualizado')
    res.json({ mesaje: 'actualizado' })
  } catch (error) {
    console.log('hubo un error', error)
  }

})

module.exports = rauteclien
const { Router } = require('express')
const connectDB = require('../dbns')
const { ObjectId } = require('mongodb');
const pro = Router()
pro.post('/obtenerlis', async (req, res, next) => {

  try {
    const conexion = await connectDB();
    const collec = await conexion.collection('productos')

    const resultado = await collec.findOne(
      // Filtro para encontrar el documento específico en el que deseas agregar el valor a la lista
      { _id: new ObjectId("65bb5c9c4bafa6f9ebf68e08") },
      // Utilizar $push para agregar un nuevo valor a la lista
      { projection: { _id: 0, producto: 1 } }
    )

    const existingProductIndex = resultado.producto.findIndex(de => de.articulo.expediente === req.body.articulo.expediente);

    if (existingProductIndex !== -1) {
      // If the product already exists in the list, update the quantity
      try {
        const conexion = await connectDB();
        const colle = await conexion.collection('productos');
        const re = await colle.updateOne(
          { _id: new ObjectId("65bb5c9c4bafa6f9ebf68e08"), "producto.articulo.expediente": req.body.articulo.expediente },
          { $inc: { "producto.$.cantidad": 1 } }
        );

        return res.json({ mensaje: 'Producto existente, cantidad actualizada.', re });
      } catch (error) {
        console.error({ mensaje: 'Error al actualizar la cantidad', error: error.message });
        return res.status(500).json({ mensaje: 'Error interno del servidor' });
      }
    } else {
      // If the product is not in the list, add it
      try {
        const conexion = await connectDB();
        const colle = await conexion.collection('productos');
        await colle.updateOne(
          { _id: new ObjectId("65bb5c9c4bafa6f9ebf68e08") },
          { $push: { producto: req.body } }
        );

        return res.json({ mensaje: 'Producto agregado a la lista.' });
      } catch (error) {
        console.error({ mensaje: 'Error al agregar el producto', error: error.message });
        return res.status(500).json({ mensaje: 'Error interno del servidor' });
      }
    }


  } catch (e) {
    return res.status(401).json({ mensaje: 'lo sentimos no se pudo hacer la consulta', error: e.message })
  }







})


pro.get('/obtenerarti', async (req, res) => {
  try {
    const conexion = await connectDB();
    const collec = await conexion.collection('productos')

    const resultado = await collec.findOne(
      // Filtro para encontrar el documento específico en el que deseas agregar el valor a la lista
      { _id: new ObjectId("65bb5c9c4bafa6f9ebf68e08") },
      // Utilizar $push para agregar un nuevo valor a la lista
      { projection: { _id: 0, producto: 1 } }
    )
    res.json(resultado.producto)
  } catch (e) {
    res.status(401).json({ mensaje: 'lo sentimos no se pudo hacer la consulta', error: e.message })
  }


})
pro.delete('/eliminarar', async (req, res) => {

  try {
    const conexion = await connectDB();
    const collec = await conexion.collection('productos')

    const resultado = await collec.updateOne(
      // Filtro para encontrar el documento específico en el que deseas agregar el valor a la lista
      { _id: new ObjectId("65bb5c9c4bafa6f9ebf68e08") },
      // Utilizar $push para agregar un nuevo valor a la lista
      { $pull: { producto: { 'articulo.expediente': req.body.expediente } } }
    )
    if (resultado.modifiedCount > 0) {
      // Verificar si se realizó alguna modificación
      res.json('Producto eliminado');
    } else {
      res.status(404).json({ mensaje: 'Producto no encontrado' });
    }
  } catch (e) {
    res.status(401).json({ mensaje: 'lo sentimos no se pudo hacer la consulta', error: e.message })
  }


})

module.exports = pro
const { Router } = require('express')
const pool = require('../db')
const fs = require('fs/promises')
const { fileup } = require('../modulodescarga')
const { json } = require('body-parser')
const routeusuario = Router()
routeusuario.post('/historialmedicamento', async (req, res) => {
  try {
    const { usuario } = req.body
    const documento = usuario.numero_documento
    console.log(documento)
    const medi = await pool.query(`select producto as medicamento,expediente as id_medicamento,forfecha_formula 
     as fecha_resetado,form_fechareclamado as fecha_reclamado ,form_estado 
     as estado,dosis,lapsotiempo,cantidaddoct 
     as cantidad ,descripcion as descripcion  from medicamentos me  inner join formula_doctor dr 
     inner join  formula_paciente pa inner join usuarios us   inner join descripcion as des on me.expediente=dr.forid_medicamento
      and  dr.foriddoct=pa.formid_formula and pa.formid_paciente=us.numero_documento  and des.iddescrip=dr.forid_descrip
      where us.numero_documento=${documento} 
      UNION  ALL
      select producto as medicamento,expediente as id_medicamento,forfecha_formula 
     as fecha_resetado,form_fechareclamado as fecha_reclamado ,form_estado 
     as estado,dosis,lapsotiempo,cantidaddoct 
     as cantidad,descripcionno as descripcion   from medicamentos me  inner join 
     formulano_doctor dr inner join  formulano_paciente pa inner join usuarionoregistrado us   
     inner join descripnoregistrado as des on me.expediente=dr.forid_medicamento and  dr.foriddoct=pa.formid_formula 
     and pa.formid_paciente=us.numero_docu and  des.desnoregistrado=dr.forid_descrip  where us.numero_docu=${documento}`)

    res.json(medi)
  } catch (error) {

    res.status(400).json({ mensaje: 'error', ero: error })
  }

})
routeusuario.post('/ciudadus', async (req, res) => {



  try {
    const ciudad = await pool.query("select nombre_ciudad from ciudades where id_ciudad=?", [req.body.id_ciudad_usuario])
    const eps = await pool.query("select nombre_entidad_prestadora from entidad_prestadora where id_entidad=?", [req.body.id_entidad_usuario])
    res.json({ ciudad: ciudad[0], eps: eps[0] })
  } catch (error) {
    res.json({ mensaje: 'error consula', ermen: error.message })
  }





})
routeusuario.post('/cargarfotoperfil', fileup, async (req, res) => {


  if (!req.file) {
    const documento = req.query.documento;
    await pool.query('update usuarios set foto_perfil=? where numero_documento=?', [null, documento])
    res.json({ con: true })
  } else {
    const da = req.file.filename.substring(0, req.file.filename.indexOf('-'))
    const documento = req.query.documento;

    await pool.query('update usuarios set foto_perfil=? where numero_documento=?', [da, documento])
    res.json({ con: true })
  }

})

routeusuario.get('/pacienteexiste', async (req, res) => {
  const { numerodocumento, nombre, apellido, motivo } = JSON.parse(req.query.paciente)
  console.log(JSON.parse(req.query.paciente))
  if ((motivo === null || motivo === '') || (numerodocumento === null || numerodocumento === '') || (nombre === null || nombre === '') || (apellido === null || apellido === '')) {

    res.status(400).json({ mensaje: 'llena todos los campos' })
  } else {
    if (/^\d{10}$/.test(numerodocumento)) {

      const us = await pool.query('select * from usuarios where numero_documento=?', [numerodocumento])
      const us2 = await pool.query('select * from usuarionoregistrado where numero_docu=?', [numerodocumento])

      if (us.length > 0) {
        if (us[0].nombre_usuario.toUpperCase() === nombre.toUpperCase() && us[0].apellido.toUpperCase() === apellido.toUpperCase()) {
          await pool.query('insert into descripcion(descripcion,motivo_documento)values(?,?) ', [motivo, numerodocumento])
          return res.json({ con: true })
        } else {

          await pool.query('insert into usuarionoregistrado(numero_docu,nombreno,apellidono)values(?,?,?)', [numerodocumento, nombre, apellido])
          await pool.query('insert into descripnoregistrado(descripcionno,motivono_documento)values(?,?) ', [motivo, numerodocumento])
          return res.json({ con: true })
        }

      }
      if (us2.length > 0) {
        await pool.query('insert into descripnoregistrado(descripcionno,motivono_documento)values(?,?) ', [motivo, numerodocumento])
        return res.json({ con: true })

      } else {
        await pool.query('insert into usuarionoregistrado (numero_docu,nombreno,apellidono)values(?,?,?)', [numerodocumento, nombre, apellido])
        await pool.query('insert into descripnoregistrado(descripcionno,motivono_documento)values(?,?) ', [motivo, numerodocumento])
        return res.json({ con: true })
      }





    } else {
      res.status(400).json({ mensaje: 'numero de documento incorrecta longitud muy corta' })
    }



  }




})
const addzeros = n => {
  if (n.toString().length < 2) return "0".concat(n)
  return n
}

routeusuario.post('/actulizarestado', async (req, res) => {

  re = await pool.query("select  distinct forfecha_formula, numero_documento,nombre_usuario, apellido,descripcion,forid_medico,nombre_hospital,nombre_especialidad from formula_doctor dr inner join usuarios u inner join formula_paciente   p  inner join descripcion des  inner join medicos me inner join hospital hos inner join especialidad es on p.formid_formula=dr.foriddoct and p.formid_paciente=u.numero_documento and dr.forid_descrip=des.iddescrip and dr.forid_medico=me.id_medico and me.id_hospital_trabajo=hos.id_hospital  and me.especialidad_medico=es.id_especialidad   where p.form_estado='pendiente' and u.numero_documento=? ", [req.body.cedula])


  res.json({ dato: re })

})

routeusuario.post('/obtenermedico', async (req, res) => {
  const r = await pool.query('select * from medicos where id_medico=?', [req.body.id])
  if (r.length > 0) {
    const r2 = await pool.query('select nombre_usuario,apellido,numero_documento from usuarios u inner join medicos me on me.id_usuario_medico=u.numero_documento where id_medico=?', [r[0].id_medico])
    res.json({ dato: r2 })

  }

})


routeusuario.post('/traermedicamentosporfecha', async (req, res) => {

  const fecha = new Date(req.body.fecha);

  // Obtener la hora, los minutos y los segundos
  let horas = addzeros(fecha.getHours());
  let minutos = addzeros(fecha.getMinutes());
  let segundos = addzeros(fecha.getSeconds());
  let años = fecha.getFullYear();
  let mes = addzeros(fecha.getMonth() + 1);
  let dia = addzeros(fecha.getDate());

  const hora12 = `${años}-${mes}-${dia} ${horas}:${minutos}:${segundos}`;
  let fechaString = fecha.toISOString();
  const hor = `${fechaString.substring(0, fechaString.indexOf('T'))} ${horas}:${minutos}:${segundos}`
  console.log(hora12)
  if (hor) {
    const r = await pool.query("select producto as medicamento,expediente as id_medicamento,forfecha_formula as fecha_resetado,form_fechareclamado as fecha_reclamado ,form_estado  as estado,dosis,lapsotiempo,cantidaddoct  as cantidad ,descripcion as descripcion  from medicamentos me  inner join formula_doctor dr  inner join  formula_paciente pa inner join usuarios us   inner join descripcion as des on me.expediente=dr.forid_medicamento  and  dr.foriddoct=pa.formid_formula and pa.formid_paciente=us.numero_documento  and des.iddescrip=dr.forid_descrip where dr.forfecha_formula=? and pa.form_estado='pendiente'", [hora12])
    res.json({ dato: r })

  }


})

routeusuario.post('/actulizarmedica', async (req, res) => {


  const fecha1 = new Date();

  // Obtener la hora, los minutos y los segundos
  let horas1 = addzeros(fecha1.getHours());
  let minutos1 = addzeros(fecha1.getMinutes());
  let segundos1 = addzeros(fecha1.getSeconds());
  let años1 = fecha1.getFullYear();
  let mes1 = addzeros(fecha1.getMonth() + 1);
  let dia1 = addzeros(fecha1.getDate());

  const hora1 = `${años1}-${mes1}-${dia1} ${horas1}:${minutos1}:${segundos1}`;

  const fecha = new Date(req.body.fecha);

  // Obtener la hora, los minutos y los segundos
  let horas = addzeros(fecha.getHours());
  let minutos = addzeros(fecha.getMinutes());
  let segundos = addzeros(fecha.getSeconds());
  let años = fecha.getFullYear();
  let mes = addzeros(fecha.getMonth() + 1);
  let dia = addzeros(fecha.getDate());

  const hora12 = `${años}-${mes}-${dia} ${horas}:${minutos}:${segundos}`;
  let fechaString = fecha.toISOString();
  const hor = `${fechaString.substring(0, fechaString.indexOf('T'))} ${horas}:${minutos}:${segundos}`

  if (hor) {
    const re = await pool.query("select formidpaciente from usuarios u inner join formula_paciente p inner join formula_doctor dr on  u.numero_documento=p.formid_paciente and p.formid_formula=dr.foriddoct  where dr.forfecha_formula=?", [hora12])
    await Promise.all(
      re.map(async (r, key) => {
        await pool.query("update formula_paciente set form_estado=?,form_fechareclamado=? where formidpaciente=? ", ['reclamado', hora1, r.formidpaciente])
      })
    )
    res.json({ mensaje: 'medicamento entregado' })
  }




})


routeusuario.post('/modificarusuario', async (req, res) => {
  console.log(req.body.datou)

  const idci = await pool.query('select id_ciudad from ciudades where  nombre_ciudad=?', [req.body.datou.ciudad])
  const entidad = await pool.query('select id_entidad from entidad_prestadora where  nombre_entidad_prestadora=?', [req.body.datou.eps])
  await pool.query('update usuarios set nombre_usuario=?,apellido=?,id_ciudad_usuario=?,direccion=?,numerotelefonico=?,id_entidad_usuario=?,tipo_usuario=? where numero_documento=? ', [req.body.datou.nombre, req.body.datou.apellido, idci[0].id_ciudad, req.body.datou.direccion, req.body.datou.numerotelefonico, entidad[0].id_entidad, req.body.datou.tipo_usuario, req.body.datou.numero_documento])

  res.json({ men: 'datos actulizados' })
})
module.exports = routeusuario


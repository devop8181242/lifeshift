const { Router } = require('express')
const pool = require('../db')
const Informacion = Router()
Informacion.get('/obtenerciudad', async (req, res) => {
    const ciudades = await pool.query('select nombre_ciudad from ciudades  order by nombre_ciudad')
    res.json(ciudades)
})

Informacion.get('/obtenereps', async (req, res) => {
    const entidad = await pool.query('select nombre_entidad_prestadora from entidad_prestadora')
    res.json(entidad)
})

module.exports = Informacion
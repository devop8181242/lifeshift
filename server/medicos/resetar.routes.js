const { Router } = require('express')
const pool = require('../db')

const medico = Router()
const addzeros = n => {
    if (n.toString().length < 2) return "0".concat(n)
    return n
}
medico.post('/resetar', async (req, res) => {

    const fecha = new Date();

    // Obtener la hora, los minutos y los segundos
    let horas = addzeros(fecha.getHours());
    let minutos = addzeros(fecha.getMinutes());
    let segundos = addzeros(fecha.getSeconds());
    let años = fecha.getFullYear();
    let mes = addzeros(fecha.getMonth() + 1);
    let dia = addzeros(fecha.getDate());

    const hora12 = `${años}-${mes}-${dia} ${horas}:${minutos}:${segundos}`;
    if (hora12) {
        const n = req.body.for
        const iddoct = await pool.query('select id_medico from medicos where id_usuario_medico=?', [req.body.documendoct])
        const pasi = await pool.query('select * from usuarios where numero_documento=?', [req.body.documen])
        const pano = await pool.query('select * from usuarionoregistrado where numero_docu=?', [req.body.documen])


        if (Array.isArray(n) && pasi.length > 0) {
            const rres = await pool.query('select iddescrip from descripcion where motivo_documento=? order by iddescrip  desc limit 1', [req.body.documen])

            await Promise.all(n.map(async (ele) => {


                await pool.query('insert into formula_doctor(forid_medico,forid_medicamento,cantidaddoct,dosis,lapsotiempo,forid_descrip,forfecha_formula)values(?,?,?,?,?,?,?)', [iddoct[0].id_medico, ele.medica.expediente, ele.medica.formula.cantidad, ele.medica.formula.dosis, ele.medica.formula.tiempo, rres[0].iddescrip, hora12])
                const id_for = await pool.query('select foriddoct from formula_doctor where forid_medico=? order by foriddoct desc limit 1', [iddoct[0].id_medico])
                await pool.query('insert into  formula_paciente(formid_paciente,formid_formula)values(?,?)', [req.body.documen, id_for[0].foriddoct])
            })
            );

            res.json({ mensaje: 'consulta exitosa' })

        } else {

            const rres = await pool.query('select desnoregistrado from descripnoregistrado where motivono_documento=? order by desnoregistrado desc limit 1', [req.body.documen])

            await Promise.all(n.map(async (ele) => {


                await pool.query('insert into formulano_doctor(forid_medico,forid_medicamento,cantidaddoct,dosis,lapsotiempo,forid_descrip,forfecha_formula)values(?,?,?,?,?,?,?)', [iddoct[0].id_medico, ele.medica.expediente, ele.medica.formula.cantidad, ele.medica.formula.dosis, ele.medica.formula.tiempo, rres[0].iddescrip, hora12])
                const id_for = await pool.query('select foriddoct from formulano_doctor where forid_medico=? order by foriddoct desc limit 1', [iddoct[0].id_medico])
                await pool.query('insert into  formulano_paciente(formid_paciente,formid_formula)values(?,?)', [req.body.documen, id_for[0].foriddoct])
            })
            );


            res.json({ mensaje: 'consulta exitosa' })
        }

    }



})
module.exports = medico
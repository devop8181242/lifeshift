const express = require('express')
const cors = require('cors')
const bodyParser = require('body-parser')
const Informacion = require('./informacion/informacion.routes')
const pro = require('./collectionproducts/product.routes')
const usurau = require('./usuario/usuario.Routes')

const multer = require('multer');
const path = require('path')
const e = require('express')



const app = express()



app.set('port', 3001)

app.use(bodyParser.json({ limit: '50mb' }));
app.use(express.static(path.join(__dirname, 'static')))

app.use(cors())
app.use(require('./routeima/imagenes.routes'))
app.use(pro)
app.use(Informacion)
app.use(usurau)
app.use(require('./registro_login/registrousuari.routes'))
app.use(require('./medicos/resetar.routes'))

app.listen(app.get('port'), (req, res) => {
    console.log(`escuchando en el puerto ${app.get('port')}`)
})





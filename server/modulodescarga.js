const multer=require('multer');
const path=require('path')
const diskstorash=multer.diskStorage({
    destination:path.join(__dirname,'./static/images'),
    filename:(req,file,cb)=>{
        
     cb(null,Date.now()+'-imagenserver-'+'.png')
    }
   })//para poder procesar imgenes middleware deja la imagen temporal mente en una carpeta
    const fileup=multer({
    storage:diskstorash,
}).single('file')

module.exports={ fileup };
const { Router } = require('express')
const fs = require('fs').promises
const pool = require('../db')
const path = require('path')
const directorioExiste = require('./verificar')
const router = Router()
router.get('/imagenes', async (req, res) => {

  const imagen = await pool.query('select * from medicamentos limit 19')
  const directorio = path.join(__dirname, '../static/dbimages/');
  console.log(directorio)
  if (!(await directorioExiste(directorio))) {
    console.log(`El directorio '${directorio}' no existe. Creándolo...`);
    try {
      await fs.mkdir(directorio, { recursive: true });
      console.log(`Directorio '${directorio}' creado correctamente.`);
    } catch (error) {
      console.error('Error al crear el directorio:', error);
    }
  }
  // Asignar permisos de escritura al directorio
  try {
    await fs.chmod(directorio, 0o777);  // 0o777 otorga permisos de lectura, escritura y ejecución a todos
  } catch (error) {
    console.error('Error al otorgar permisos de escritura al directorio:', error);
  }

  const imagere = await fs.readdir(path.join(__dirname, '../static/dbimages/'))

  try {

    await Promise.all(

      imagen?.map(async (da, key) => {


        try {

          /* const exites = () => {
            return new Promise((resolve, reject) => {
               const existingProductIndex = imagere.findIndex(de => de === `${da.expediente}monkeywit.png`);
               if (existingProductIndex === -1) {
                 reject(existingProductIndex);
               } else {
                 resolve(existingProductIndex);
               }
             });
           };
           
           const resulta=await exites() */
          const existingProductIndex2 = imagere.findIndex(de => de === `${da.expediente}monkeywit.png`);

          if (existingProductIndex2 === -1) {
            await fs.writeFile(
              path.join(
                __dirname,
                '../static/dbimages/' +
                da.expediente +
                'monkeywit.' +
                'png'
              ),
              da.imagenes_medicamento,
              { encoding: 'binary' }
            );
            console.log(da.imagenes_medicamento)
          }

          //console.log(`Imagen  guardada correctamente.`);
        } catch (error) {

          console.error(`Error al guardar la imagen `, error);
        }


      })
    );

    const imagered = await fs.readdir(path.join(__dirname, '../static/dbimages/'))//lee que archivos hay en un directorio y retorna el nombre de  cada uno de esos archivo una rray




    res.json({ imagen, imagered })



  } catch (error) {
    console.error('Error al procesar la solicitud:', error);
    res.status(500).send('Error interno del servidor');
  }
})

router.get('/busquedad', async (req, res) => {
  let re = req.query.q
  try {
    re = await pool.query('select producto from medicamentos where producto like ? limit 10', [`${re}%`])
    res.json({ respu: re })


  } catch (error) {
    res.status(400).json({ mesaje: 'error al hacer la consulta', mensajeer: error.message })
  }
})

router.get('/busqueda', async (req, res) => {
  let re = req.query.medicamento

  try {
    re = await pool.query('select * from medicamentos where producto like ? limit 20', [`${re}%`])

    if (re) {
      const directorio = path.join(__dirname, '../static/dbimages/');
      if (!(await directorioExiste(directorio))) {
        console.log(`El directorio '${directorio}' no existe. Creándolo...`);
        try {
          await fs.mkdir(directorio, { recursive: true });
          console.log(`Directorio '${directorio}' creado correctamente.`);
        } catch (error) {
          console.error('Error al crear el directorio:', error);
        }
      }
      // Asignar permisos de escritura al directorio
      try {
        await fs.chmod(directorio, 0o777);  // 0o777 otorga permisos de lectura, escritura y ejecución a todos
      } catch (error) {
        console.error('Error al otorgar permisos de escritura al directorio:', error);
      }

      const imagere = await fs.readdir(path.join(__dirname, '../static/dbimages/'))

      try {

        await Promise.all(

          re?.map(async (da, key) => {


            try {

              /* const exites = () => {
                return new Promise((resolve, reject) => {
                   const existingProductIndex = imagere.findIndex(de => de === `${da.expediente}monkeywit.png`);
                   if (existingProductIndex === -1) {
                     reject(existingProductIndex);
                   } else {
                     resolve(existingProductIndex);
                   }
                 });
               };
               
               const resulta=await exites() */
              const existingProductIndex2 = imagere.findIndex(de => de === `${da.expediente}monkeywit.png`);

              if (existingProductIndex2 === -1 && da.imagenes_medicamento !== null) {
                await fs.writeFile(
                  path.join(
                    __dirname,
                    '../static/dbimages/' +
                    da.expediente +
                    'monkeywit.' +
                    'png'
                  ),
                  da.imagenes_medicamento,
                  { encoding: 'binary' }
                );
                console.log(da.imagenes_medicamento)
              }

              //console.log(`Imagen  guardada correctamente.`);
            } catch (error) {

              console.error(`Error al guardar la imagen `, error);
            }


          })
        );


        res.json({ respu: re })


      } catch (error) {
        console.error('Error al procesar la solicitud:', error);
        res.status(500).send('Error interno del servidor');
      }
    }

  } catch (error) {
    res.status(400).json({ mesaje: 'error al hacer la consulta', mensajeer: error.message })
  }
})
module.exports = router
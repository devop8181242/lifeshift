const fs=require('fs/promises')
async function directorioExiste(directorio) {
    try {
      await fs.access(directorio, fs.constants.F_OK);
      return true; // El directorio existe
    } catch (error) {
      return false; // El directorio no existe
    }
  }
  module.exports=directorioExiste
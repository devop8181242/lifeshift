const {MongoClient}=require('mongodb')

const url='mongodb://localhost:27017/productos'
// Objeto para almacenar la conexión de la base de datos
let db;

// Función para conectar a la base de datos
async function connectDB() {
  try {
    if (!db) {
      const client = await MongoClient.connect(url, { useNewUrlParser: true, useUnifiedTopology: true });
      db = client.db('productos');
    }
    return db;
  } catch (error) {
    console.error('Error al conectar a MongoDB:', error);
    throw error;
  }
}

module.exports = connectDB;